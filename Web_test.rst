Connection Test
-------------------



Unittest
----------

Mocha
^^^^^^^

    test/test.js

    .. code-block :: javascript

        var assert = require("assert");
        describe('Array', function(){
            describe('#indexOf()', function(){
                it('should return -1 when the value is not present', function(){ // Error display string
                    assert.equal(-1, [1,2,3].indexOf(5));
                    assert.equal(-1, [1,2,3].indexOf(0));
                });
            });
        });

        describe('Array', function(){
            describe('#indexOf()', function(){
                it('should return -1 when the value is not present', function(){
                    [1,2,3].indexOf(5).should.equal(-1);
                    [1,2,3].indexOf(0).should.equal(-1);
                });
            });
        });





資源連結
"""""""""

    官網： http://visionmedia.github.io/mocha/

Locust
^^^^^^^^

    Loading test tool


Install
""""""""""

    ::

        pip install locustio

    若要測試多程序的則安裝pyzmq

    ::

        pip install pyzmq

        or

        easy_install pyzmq

    start sample

    .. code-block :: python

        from locust import HttpLocust, TaskSet

        def login(I):
            I.client.post('/login', {"username": "ellen_key", "password":"education"})

        def index(I):
            I.client.get('/')

        def profile(I):
            I.client.get('/profile')


        class UserBehavior(TaskSet):
            tasks = {index: 2, profile: 1}

            def on_start(sself):
                login(self)

        class WebsiteUser(HttpLocust):
            task_set = UserBehavior
            min_wait = 5000
            max_wait = 9000
