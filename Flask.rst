.. sphinx documentation master file, created by
   sphinx-quickstart on Wed Apr 10 10:30:15 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Flask
======

Form
-----

Basic Field 使用範例說明：
--------------------------------------------------------------------------

    +------------------------+-----------------------------------------+
    | BooleanField           | reuturn a tag <input type="checkbox">   |
    +========================+=========================================+
    | DateField              | default：%Y-%m-%d                       |
    +------------------------+-----------------------------------------+
    | DateTimeField          | default：%Y-%m-%d %H:%M:%S              |
    +------------------------+-----------------------------------------+

    +----------+-------------------------------------------------------------------------------+
    |          |File upload tag                                                                |
    |          |if any is sent by the browser in the post params                               |
    |FileField |This field will NOT actually handle the file upload portion                    |
    |          |as wtforms does not deal with individual frameworks’ file handling capabilities|
    +----------+-------------------------------------------------------------------------------+

    .. code-block:: python

        class UploadForm(Form):
            image        = FileField(u'Image File', [validators.regexp(u'^[^/\\]\.jpg$')])
            description  = TextAreaField(u'Image Description')

            def validate_image(form, field):
                if field.data:
                    field.data = re.sub(r'[^a-z0-9_.-]', '_', field.data)

        def upload(request):
            form = UploadForm(request.POST)
            if form.image.data:
                image_data = request.FILES[form.image.name].read()
                open(os.path.join(UPLOAD_PATH, form.image.data), 'w').write(image_data)

Validators
-----------------------------------------------------------------------------

    ::

        class wtforms.validators.ValidationError(message=u'', *args, **kwargs):

    WTFofrm 送出的時候會先進行驗證與檢查，可以新增一些檢查的method在class底下，若是不通過驗會產生ValidationError。

    class wtforms.validators.EqualTo(fieldname, message=None)：

        解決兩個欄位的值必須要相同的時候的驗證，常利用於申請帳號時確認密碼使用。

    class wtforms.validators.Length(min=-1, max=-1, message=None)：

        檢查Max 與 Min 長度，而message則是Error message

    class wtforms.validators.NumberRange(min=None, max=None, message=None):

        檢查數字的區段，若不在Min與Max的區段之內則回傳Error message。

    EqualTo

        demo

        .. code-block:: python

            class ChangePassword(Form):
                password = PasswordField(u'New Password',
                    [InputRequired(), EqualTo(('confirm', message = u'Passwords must match')])
                confirm = PasswordField(u'Repeat Password')

Custom validators
---------------------------------------------------------------------------------

    .. code-block:: python

        class MyForm(Form):
        name = TextField('Name', [Required()])

        def validate_name(form, field):
            if len(field.data) > 50:
                raise ValidationError('Name must be less than 50 characters')

    name 送出的時候會檢查字串是否大於50個字元，若超過50個字元則停止送出表單並且產生Error message

    .. code-block:: python

        def my_length_check(form, field):
            if len(field.data) > 50:
                raise ValidationError('Field must be less than 50 characters')

        class MyForm(Form):
            name = TextField('Name', [Required(), my_length_check])

    name 送出的時候執行my_length_check method檢查字串是否大於50個字元，若超過50個字元則停止送出表單並且產生Error message

Removing Fields per-instance
------------------------------

    可以在某些特定的狀態下，使用del刪除表單內的欄位。

    .. code-block :: python

        #-*- coding:utf-8 -*-
        from flask import Flask, render_template, request
        from forms import CustomValidatorForm

        app = Flask(__name__)

        DEBUG = True
        CSRF_ENABLED = True
        SECRET_KEY = 'you-will-never-guess'
        RECAPTCHA_PUBLIC_KEY='6LflTuASAAAAAOvtAurqYAu-H6tv-RB_9XufQTWY'
        app.config.from_object(__name__)

        @app.route('/', methods=['GET', 'POST'])
        def index():
            form = CustomValidatorForm()
            if form.validate_on_submit():
                del form.name
                return render_template('del_demo.html', form=form)
            return render_template('validator.html',
                form = form)
        if '__main__' == __name__:
            app.run()

    del_demo.html

    .. code-block :: html

        <!DOCTYPE HTML>
        <html>
            <head></head>
            <body>
                <form action='' method='POST'>
                    <fieldset>
                        {{ form.hidden_tag() }}
                        {% if form.name %}
                            name: {{ form.name }} <br />
                        {% endif %}
                        <ul>
                            {% for error in form.name.errors %}
                                {% if error %}
                                    <li>
                                        <p>{{ error }}</p>
                                    </li>
                                {% endif %}
                            {% endfor %}
                        </ul>
                        password : {{ form.pwd }}
                        <ul>
                            {% for error in form.pwd.errors %}
                                {% if error %}
                                    <li>
                                        <p>{{ error }}</p>
                                    </li>
                                {% endif %}
                            {% endfor %}

                        </ul>
                        multicheckbox : {{ form.checkbox }}
                        <input type='submit' value='送出'>
                    </fieldset>
                </form>
            </body>
        </html>


Field
----------

    * errors

        type : tuple

    * process_errors

        type : tuple

    * raw_data

        type : None

    * validators

        type : tuple

    * widget

        type : None

    * _formfield

        type : true

    * _translations

        value : DummyTranslations()

widget
----------

    預設的widget

    ::
        class wtforms.widgets.ListWidget(html_tag='ul', prefix_label=True)




Werkzeug
----------------------------------------------

    Werkzeug是一個實用的python 函式庫。包含了debugger

    demo.py:

    ::

        from werkzeug.wrappers import Request, Response
        from werkzeug.serving import run_simple

        @Request.application
        def application(request):
            return Response('Hello World!')

        if __name__ == '__main__':
            run_simple('localhost', 4000, application)


INSTALL
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    ::

        pip install Werkzeug

virtualenv
""""""""""""""""""""""""""""""""""""""""""""""""

            ::

                pip install virtualenv

            因為使用Werkzeug開發的web application會有很多個，可以利用
            virtualenv 對個別的web設定個別的執行python環境。

Jinja2 template and Database redis
""""""""""""""""""""""""""""""""""""""""""""""""

    ::

        pip install jinja2

        sudo apt-get install redis-server




uWSGI
------------------------------

1. 簡介
^^^^^^^^^^^^^^^^^^^

    WSGI_ 是(Python Web Server Gateway Interface 的縮寫)專為Python所設計的一個
    的 Gateway_ Interface，它的功能在Server與Application或是framwork溝通時的橋樑。

    而uWSGI則是一個協助使用Server 端的 WSGI 一個套件 。**deployment option??**

    uWSGI 是一種協議同時也可以是一個應用程式伺服器， 可以支援 FastCGI_ 和HTTP協議。
    與uWSGI類似的套件有 nginx_ , lighthttpd_ , cherokee_ 。

    .. _nginx : http://nginx.org/

    .. _lighthttpd : http://www.lighttpd.net/

    .. _cherokee : http://www.cherokee-project.com/

    .. _WSGI : http://en.wikipedia.org/wiki/Web_Server_Gateway_Interface

    .. _Gateway : http://en.wikipedia.org/wiki/Gateway_(telecommunications)

    .. _FastCGI : http://flask.pocoo.org/docs/deploying/fastcgi/#deploying-fastcgi

    .. note:

        在執行app.run()這個函式之前一定要先確定程式的進入點是否為此模組，可以利用
        if __name__ = '__main__' 判斷，或是將其分離至其他檔案，否則啟動的都只
        會是本機端的伺服器。

2.安裝
^^^^^^^^^^^^^^^^^^^^^

    安裝 **build-essential** 與 python-dev:

    ::

        $ sudo apt-get install build-essential python-dev

    安裝uwsgi:

    ::

        $ sudo pip install uwsgi

    或是

    ::

        $ curl http://uwsgi.it/install | bash -s default /tmp/uwsgi

    此指令會將uwsgi安裝在/tmp/uwsgi 裡

    或是

    ::

        $ wget http://projects.unbit.it/downloads/uwsgi-latest.tar.gz
        $ tar zxvf uwsgi-latest.tar.gz
        $ cd <dir>
        $ make

3.DEMO
^^^^^^^^^^^^^^^^^^^^^

    test.py:

    ::

        def application(env, start_response):
            start_response('200 OK', [('Content-Type','text/html')])
            return "Hello World"

    這個檔案只有一個函式，因為uWSGI python loader預設搜尋的函式名稱就是 **application**

\ **Start**\

    ::

        $ uwsgi --http :9090 --wsgi-file test.py

    --processes and --threads option:

    ::

        $ uwsgi --http :9090 --wsgi-file test.py --processes 4 --threads 2 --stats 127.0.0.1:9191

    上述指令可以同時開啟四個processes，可以在終端機輸入ps -ax檢查，並起在本機的9191 port
    可以看到關於此四個processes的作業狀態。

    **threads不知道怎麼測試！囧！**

\ **configuration:**\


    uwsgi的config的副檔名是.ini:test_conf.ini

    ::

        [uwsgi]
        socket = /tmp/uwsgi
        http = :9090
        chdir = /home/horsekit/Dropbox/www/test/
        pythonpath = test.py
        module = main
        master = true
        processes = 4
        threads = 2
        stats = 127.0.0.1:9191

    **這個還有bug**

    設定檔存放位置在：  /etc/uwsgi/apps-available/

4.參考資料：
^^^^^^^^^^^^^^^^^^^^^

    flask ： http://flask.pocoo.org/docs/deploying/uwsgi/

    快速安裝uwsgi 與 DEMO : http://uwsgi-docs.readthedocs.org/en/latest/WSGIquickstart.html

    uwsgi config : https://uwsgi-docs.readthedocs.org/en/latest/Configuration.html?highlight=config


Restful
----------------------------------------------------

    等待新增

簡介
^^^^^^^^^^^^^^^^^^^^

    REST Representational state transfer的縮寫，是近期為網頁與API所設計的結構規範。

    REST 系統主要有六個設計原則：

        **Client-Server** : 必須要有明確的server與client，Server提供服務給Client使用

        **Stateless** ： Client 送出請求石壁需要包含所有server需要的資訊，換句話說在接收到client的每一個請求，server都不需要儲存任何資訊。

        **Cacheable** : The server must indicate to the client if requests can be cached or not.

                        Client的請求都必須處存在cache裡。

        **Layered System** ： Communication between a client and a server should be standardized in such a way that allows intermediaries to respond to requests instead of the end server, *without the client having to do anything different.*

                            Client和Server的溝通需要標準化，處理Client的請求是中介機構而不是後端的Server，和一般的客戶端不需要作任何事情的處理方式不同。

        **uniform interface** : Server與Client的溝通方式必須要為unform。

        **Code on demand** :  Servers can provide executable code or scripts for clients to execute in their context. This constraint is the only one that is optional.

        DEMO RESTful requests

        .. code-block:: python

            import requrests
            r = requests.get('http:api.github.com/events')
            postr = requests.post("http://httpbin.org/post")
            headr = requests.head("http://httpbin.org/get")
            optionsr = requests.options("http://httpbin.org/get")


參考資料
-----------------------------------------------------

    光頭DEMO:http://blog.miguelgrinberg.com/post/designing-a-restful-api-with-python-and-flask



frozen-flask
---------------------------------------------------

    依照設定可以產生相對應的static file 儲存於build資料夾中！

    demo

    .. code-block :: python

        from flask import Flask, render_template
        from flask_frozen import Freezer

        app = Flask(__name__)

        freezer = Freezer(app)

        @app.route('/', methods=['GET', 'POST'])
        def products_list():
            return render_template('index.html')

        @app.route('/product_<int:product_id>/', methods=['GET', 'POST'])
        def product_details(product_id):
            return render_template('product.html',product_id = product_id)


        @freezer.register_generator
        def product_details():  # endpoint defaults to the function name
            # `values` dicts
            yield {'product_id': '1'}
            yield {'product_id': '2'}
            yield {'product_id': '3'}

        @freezer.register_generator
        def product_url_generator():  # Some other function name
            # `(endpoint, values)` tuples
            yield 'product_details', {'product_id': '1'}
            yield 'product_details', {'product_id': '2'}

        @freezer.register_generator
        def product_url_generator():
            yield '/Tomas_1/'
            yield '/Tomas_2/'

        @freezer.register_generator
        def product_url_generator():
            # Return a list. (Any iterable type will do.)
            return [
                '/product_1/',
                # Mixing forms works too.
                ('product_details', {'product_id': '2'}),
            ]

        if __name__ =='__main__':

            freezer.run(debug=True)

Configure
^^^^^^^^^^^^^^

    等待新增

參考資料
--------------------------------------------------

    frozen-flask : http://pythonhosted.org/Frozen-Flask/


Flask-wtf
--------------------------------

    等待新增

HTML5 widgets
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    等待新增

Flask View
---------------------------------------------------

    等待新增


Flask-Upload
-----------------------------------------

    HTML form 表單中檔案上傳時要新增屬性 enctype=multipart/form-data

    demo:

    myapp.py

    ::

        import os
        from flask import Flask, request, redirect, url_for, render_template,\
            send_from_directory
        from werkzeug import secure_filename, SharedDataMiddleware


        UPLOAD_FOLDER = '/data'
        ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

        myapp = Flask(__name__)
        DEBUG = True


        myapp.config.from_object(__name__)

        myapp.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
        myapp.add_url_rule('/uploads/<filename>', 'uploaded_file',
                         build_only=True)
        myapp.wsgi_app = SharedDataMiddleware(myapp.wsgi_app, {
            '/uploads':  myapp.config['UPLOAD_FOLDER']
        })

        def allowed_file(filename):
            return '.' in filename and \
                   filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

        @myapp.route('/', methods=['GET', 'POST'])
        def upload_file():
            if request.method == 'POST':
                file = request.files['file']
                if file and allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    file.save(os.path.join(myapp.config['UPLOAD_FOLDER'], filename))
                    return redirect(url_for('uploaded_file',
                                            filename=filename))
            return render_template('index.html')

        @myapp.route('/uploads/<filename>')
        def uploaded_file(filename):
            return send_from_directory(app.config['UPLOAD_FOLDER'],
                                       filename)
        myapp.run()


    templates/index.html

    ::

        <!doctype html>
        <html>
            <head>
                <title>Flask upload Demo</title>
            </head>
            <body>
                <form action="" method='post' enctype='multipart/form-data'>
                    <p><input type=file name=file>
                    <input type=submit value=Upload>
                </form>
            </body>
        </html>


Flaskr Unittest
------------------------------

    flask-pymongo unittest DEMO



    Test Skeleten Demo

    .. code-block :: python

        #-*- encoding:utf-8 -*-
        """
        exam online 測試
        """
        import os
        import unittest
        import tempfile
        from pymongo import MongoClient
        from app import app
        import json

        class FlaskTestCase(unittest.TestCase):

            def setUp(self):
                self.cls = app.test_client()

            def tearDown(self):
                self.conn.close()

            def test_conn_db(self):
                try:
                    self.conn = MongoClient()
                except:
                    raise "MongoDB Connection Error!"

            def test_write_exam(self):
                self.db = self.conn['test_exam']
                self.papers = self.db['papers']
                json_file = open('examtest.json', 'r')
                DB_data = json.loads(json_file.read())
                try:
                    self.papers.insert(DB_data)
                except:
                    raise "MongoDB Insert Error!"

            def test_exam_paper(self):
                pass

        if __name__ == "__main__":
            unittest.main()




參考網址
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

官方連結：http://flask.pocoo.org/docs/testing/

Flask-Skeleton
----------------------------

    flask login 以MVC架構完成的skeleton

    config.py

    ::

        CSRF_ENABLED = True
        SECRET_KEY = "DO IT"
        DEBUG = True
        MONGO_DBNAME = 'login'

    run.py

    ::

        from myapp import app
        if '__main__' == __name__:
            app.run()

    myapp/__init__.py

    ::

        #-*- encoding: utf-8 -*-

        from flask import Flask
        from flask.ext.pymongo import PyMongo
        from flask.ext.admin import Admin

        app = Flask(__name__)

        app.config.from_object('config')

        mongo = PyMongo(app)

        from myapp import views

        from login import init_login
        init_login()


        from admin_views import MyAdminIndexView
        admin = Admin(app, u'start/ending 管理介面',  index_view=MyAdminIndexView())

    myapp/views.py

    ::

        from myapp import app

        from flask import render_template,flash, url_for, redirect, request

        from flask.ext.login import login_user, current_user, \
                                    logout_user, login_required,  \
                                    fresh_login_required

        from forms import LoginForm
        from models import User

        @app.route('/')
        def index():
            return render_template('index.html', user=current_user)

        @app.route('/secret')
        @fresh_login_required
        def secret():
            return render_template("secret.html")

        @app.route('/login', methods=('GET', 'POST'))
        def login_view():
            form = LoginForm()
            if form.validate_on_submit():
                user = User(form.get_user())
                login_user(user)
                return redirect(request.args.get("next") or url_for('index'))
            return render_template('form.html', form=form)

        @app.route('/logout')
        @login_required
        def logout_view():
            logout_user()
            return redirect(url_for('index'))

    myapp/forms.py

    ::

         #-*- encoding: utf-8 -*-
        from myapp import  mongo

        from flask.ext.wtf import Form, ValidationError
        from wtforms import TextField, PasswordField
        from wtforms.validators import Required

        class LoginForm(Form):
            login = TextField("name", validators=[Required(message=u"需要輸入使用者ID")])
            pwd = PasswordField("password", validators=[Required(message=u"需要輸入使用者密碼")])

            def validate_login(self, field):
                user = self.get_user()

                if user is None:
                    raise ValidationError(u'使用者帳號錯誤!')

                if user['pwd'] != self.pwd.data:
                    raise ValidationError(u'使用者密碼錯誤!')

            def get_user(self):
                #用login user name 去撈資料庫看有沒有這樣的user
                return mongo.db.users.find_one({'name':self.login.data})

    myapp/login.py

    ::

        from myapp import app

        from myapp import mongo
        from models import User

        from flask.ext.login import LoginManager

        def init_login():
            login_manager = LoginManager()
            login_manager.setup_app(app)

            @login_manager.user_loader
            def load_user(id):
                return User(mongo.db.users.find_one({"_id":id}))

    myapp/models.py

    ::

        #-*- encoding: utf-8 -*-
        class User(object):
            def __init__(self, user):
                self.id = user['_id']
                self.name = user['name']
                self.pwd = user['pwd']

            def is_authenticated(self):
                return True

            def is_active(self):
                return True

            def is_anonymous(self):
                return False

            def get_id(self):
                return self.id

            #給flask-admin管理介面的必備項目
            def __unicode__(self):
                return self.username

    myapp/admin_views.py

    ::

        from flask.ext.login import current_user
        from flask.ext.admin import  AdminIndexView

        class MyAdminIndexView(AdminIndexView):
            def is_accessible(self):
                return current_user.is_authenticated()


Flask-script
--------------------------------

    script 可以協助設定你的資料庫或處理固定時程的工作，

    DEMO

    myapp.py

    .. code-block :: python

        from flask import Flask
        app = Flask(__name__)

        @app.route("/")
        def hello():
            return "Hello World!"

        if __name__ == "__main__":
            app.run()

    script-demo.py

    .. code-block :: python

        from flask.ext.script import Manager
        from myapp import app

        manager = Manager(app)

        @manager.command
        def hello():
            print "hello"
        @manager.command
        def hello2():
            print "hello2"

        if __name__=='__main__':
            manager.run()

    commond 執行時

    ::

        $ python script-demo.py hello #執行函數名稱

    option

    script-demo.py

    .. code-block :: python

        from flask.ext.script import Manager
        from myapp import app

        manager = Manager(app)

        @manager.option('-n', '--name', help='Your name')
        def hello(name):
            print "hello", name

        if __name__=='__main__':
            manager.run()

    執行

    DEMO

    ::

        $ python script-demo.py hello -n 'Tomas'    # hello Tomas
        $ python manage.py hello --name=Joe #hello Joe


    script-demo.py

    .. code-block :: python

        from flask.ext.script import Server, Manager
        from myapp import create_app

        manager = Manager(create_app)
        manager.add_command("runserver", Server(host="0.0.0.0", port=9000))

        if __name__ == "__main__":
            manager.run()

    執行

    ::

        $ python script-demo.py runserver

Flask-pymongo
--------------------------

作者:Tomas

1. 安裝flask-pymongo
^^^^^^^^^^^^^^^^^^^^^

::

    # pip install Flask-PyMongo

2. 使用flask-pymongo
^^^^^^^^^^^^^^^^^^^^^

::

    from flask import Flask
    from flask.ext.pymongo import PyMongo

    app = Flask(__name__)
    mongo = PyMongo(app)

PyMongo預設連線MongoDB server為logcalhost  port為27017，
預設的database name是app.name。

::

    @app.route('/')
    def home_page():
        online_users = mongo.db.users.find({'online': True})
        return render_template('index.html',
            online_users=online_users)

搜尋資料庫資料並且顯示在template上。

``Collection.find_one_or_404(*args, **kwargs):``

::

    @app.route('/user/<username>')
    def user_profile(username):
        user = mongo.db.users.find_one_or_404({'_id': username})
        return render_template('user.html',
            user=user)


若有找到則回傳資料，若搜尋為空則回傳錯誤訊息。

Configuration:

=========================   ============================================
MONGO_URI                   MongiDB 的URI 位址
MONGO_HOST                  Host name or  IP address 。預設為localhost
MONGO_PORT                  MongoDB所監聽的port
MONGO_AUTO_START_REQUEST    Set to False to disable PyMongo 2.2’s “auto
                            start request” behavior (see MongoClient).
                            Default: True.

MONGO_MAX_POOL_SIZE         MonogoDB的連線流量大小限制
MONGO_DBNAME                Database 的名稱
MONGO_USERNAME              使用者名稱。預設:None
MONGO_PASSWORD              MongoDB 的密碼。預設:None
MONGO_REPLICA_SET           資料庫資料是否可以被copy
MONGO_READ_PREFERENCE       Determines how read queries are routed to
                            the replica set members.
                            Must be one of PRIMARY, SECONDARY, or
                            SECONDARY_ONLY, or the string names thereof.
                            Default PRIMARY.

MONGO_DOCUMENT_CLASS        PyMongo回傳的資料型態。預設:dict
=========================   ============================================

多重連接範例:

::

    app = Flask(__name__)

    # connect to MongoDB with the defaults
    mongo1 = PyMongo(app)

    # connect to another MongoDB database on the same host
    app.config['MONGO2_DBNAME'] = 'dbname_two'
    mongo2 = PyMongo(app, config_prefix='MONGO2')

    # connect to another MongoDB server altogether
    app.config['MONGO3_HOST'] = 'another.host.example.com'
    app.config['MONGO3_PORT'] = 27017
    app.config['MONGO3_DBNAME'] = 'dbname_three'
    mongo3 = PyMongo(app, config_prefix='MONGO3')

3. 參考資料:
https://flask-pymongo.readthedocs.org/en/latest/


flask-openid
----------------------

1. 安裝flask-openid
^^^^^^^^^^^^^^^^^^^^

::

     # easy_install Flask-OpenID

或是

::

    # pip install Flask-OpenID

2. 使用方法
^^^^^^^^^^^^^^^^^^^^

::

    from flaskext.openid import OpenID
    oid = OpenID(app, '/path/to/store')

OpenID預設是以檔案類型儲存認證資料，連線認證資料儲存路徑設定於/path/to/store，也可以選擇儲存於資料庫內，
路徑也可以使用OPENID_FS_STORE_PATH變數進行設定。
登入的使用者可以利用session記錄儲存，key為'openid'。
範例如下:

::

    from flask import g, session

    @app.before_request
    def lookup_current_user():
        g.user = None
        if 'openid' in session:
            g.user = User.query.filter_by(openid=openid).first()


User這個範例是使用SQLAlchemy，只要上一章的Database修改為Pymongo就可以使用!
Loginhandler():
是在flask-login中的function，協助處理登入時的基本狀況:

1. 是否有儲存登入資訊，若是有則會利用get_next_url()可以直接得到帳號資料。

2. 當使用者嘗試登入，送出帳號密碼資料後，會收到回傳的openid或是錯誤訊息。

3. 認證失敗會回傳失敗訊息，可以使用fetch_error() 來得到錯誤訊息!

Login template:

::

    {% extends "layout.html" %}
    {% block title %}Sign in{% endblock %}
    {% block body %}
    <h2>Sign in</h2>
    <form action="" method=post>
        {% if error %}<p class=error><strong>Error:</strong> {{ error }}</p>{% endif %}
        <p>
        OpenID:
        <input type=text name=openid size=30>
        <input type=submit value="Sign in">
        <input type=hidden name=next value="{{ next }}">
    </form>
    {% endblock %}

after_login():

::

    from flask import flash
    @oid.after_login
    def create_or_login(resp):
        session['openid'] = resp.identity_url
        user = User.query.filter_by(openid=resp.identity_url).first()
        if user is not None:
            flash(u'Successfully signed in')
            g.user = user
            return redirect(oid.get_next_url())
        return redirect(url_for('create_profile', next=oid.get_next_url(),
                                name=resp.fullname or resp.nickname,
                                email=resp.email))


若是登入成功，帳號資訊會儲存在resp裡面，若是曾經將OpenID儲存在資料庫中，可以去資料庫撈取OpenID，若是失敗則導向回使用者頁面。

3. 顯示你的帳號資訊:
^^^^^^^^^^^^^^^^^^^^

::

    @app.route('/create-profile', methods=['GET', 'POST'])
    def create_profile():
        if g.user is not None or 'openid' not in session:
            return redirect(url_for('index'))
        if request.method == 'POST':
            name = request.form['name']
            email = request.form['email']
            if not name:
                flash(u'Error: you have to provide a name')
            elif '@' not in email:
                flash(u'Error: you have to enter a valid email address')
            else:
                flash(u'Profile successfully created')
                db_session.add(User(name, email, session['openid']))
                db_session.commit()
                return redirect(oid.get_next_url())
        return render_template('create_profile.html', next_url=oid.get_next_url())


create_profile.html

::

    {% extends "layout.html" %}
    {% block title %}Create Profile{% endblock %}
    {% block body %}
    <h2>Create Profile</h2>
    <p>
        Hey!  This is the first time you signed in on this website.  In
        order to proceed we need a couple of more information from you:
    <form action="" method=post>
        <dl>
        <dt>Name:
        <dd><input type=text name=name size=30 value="{{ request.values.name }}">
        <dt>E-Mail:
        <dd><input type=text name=email size=30 value="{{ request.values.email }}">
        </dl>
        <p>
        <input type=submit value="Create profile">
        <input type=hidden name=next value="{{ next }}">
    </form>
    <p>
        If you don't want to proceed, you can <a href="{{ url_for('logout')
        }}">sign out</a> again.
    {% endblock %}

Logging Out:

::

    @app.route('/logout')
    def logout():
        session.pop('openid', None)
        flash(u'You were signed out')
        return redirect(oid.get_next_url())


4. Responsive
^^^^^^^^^^^^^^^^^^^^

===============  ==========================
aim              AIM messenger address as string
===============  ==========================
blog             Blog 網址(string)
country          國家名稱
date_of_birth    生日
email            email位址
fullname         使用者完整名稱
gender           性別
icq              Icq 帳號
identity_url     Openid (登入資訊)
image            使用者頭相照片
**jabber**           **jabber address as string**
language         使用者預設語言
month_of_birth   生日月份
msn              MSN位址
nickname         使用者暱稱
phone            電話號碼
**Postcode**     **free text that should conform to the user’s country’s**
                 **postal system**
skype            SKYPE 帳號
timezone         區域時間
website          Web的網址
yahoo            Yahoo即時通的位址
year_of_birth    生日的年份
===============  ==========================

參考資料:
http://pythonhosted.org/Flask-OpenID/
http://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-v-user-logins


Flask Mongoengine
-----------------------------------------------------------



Flask-Login
------------------------------

簡介：
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    \ **Flask-login**\ 在Flask中負責登入，登出機制控管以及某個允許的時間區段內在 session_ 中儲存使用者的相關資訊的功能。

    Flask-login 可以

        在session中儲存使用者的ID，當使用者在登入或是登出的時候更為便利。

        設定使用者可以使用的view。

        簡單的完成 rememberme 的動作

        避免hack使用cookie登入，保護網站安全。

        整合 Flask-principal 或是其他驗證機制擴展功能。

        不限制驗證方法登入，你可以配合自用的登入機制或是OpenID或是其他方式登入。

    Flask-login 無法做到的事情

        並不強制綁定一個資料庫，或是加入一個儲存的函式。\ **Flask-login**\只是單純的取得使用者資訊。

        只處理登入登出之外沒有其他處理權限的功能。

        無法處理註冊或是恢復帳號等功能。

安裝flask-login
^^^^^^^^^^^^^^^^^

    ::

        $ pip install flask-login


初始化設定你的Application
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    ::

        login_manager = LoginManager()
        login_manager.init_app(app)

        #init_app在協助你設定你的application中的before_request以其after_request並且利用loginmanager的class產生一個app.login_manager的物件。


User class:
^^^^^^^^^^^^^

    ::

        class User(UserMixin):
            def __init__(self, name, id, active=True):
                self.name = name
                self.id = id
                self.active = active

            def is_active(self):
                return self.active

    .. note ::

        User class是協助處理帳號權限，他可以辨別訪客，會員的停權與驗證，以及取得會員的ID，而UserMixin則是flask-login已經寫好的基本User class，可以
        透過繼承它來簡化自己的User class。

    ::

        USERS = {
            1: User(u"Notch", 1),
            2: User(u"Steve", 2),
            3: User(u"Creeper", 3, False),
        }

        USER_NAMES = dict((u.name, u) for u in USERS.itervalues()) #利用迴圈產生一個物件，手法應用

        #模擬Database

    ::

        @login_manager.user_loader
        def load_user(userid):
            return USERS.get(int(userid))

            #login_manager.user_loader 要傳入一個unickod的ID，它會檢查session中是否有user，若有則回傳一個物件若不存在則回傳None。

    ::

        login_user(USER_NAMES[username])

        #嘗試user login的動作，傳入的參數是利用User class產生的物件，會檢查method is_active的回傳值若是True則會登入成功，若是False則登入失敗。

    ::

        @login_required

        #這是一個decorate function ，放置於需要進行驗證的頁面，若你尚未登入他會執行unauthorized()，回傳一個錯誤訊息，阻擋使用者進入頁面。

    ::

        logout_user()

        #將使用者在session中的資料移除，並且若是cookie中有存使用者資料也會一併移除。


    ::

        class Anonymous(AnonymousUser):
        name = u"Anonymous"

        #訪客的class，AnonymousUser也是在flask-login已經幫你寫好了一個匿名帳號專用的class只要繼承它的method就可以簡單的作使用。

    customize the process further

    .. code-block:: python

        @login_manager.unauthorized_handler
        def unauthorized():
            #do stuff
            return a_response

完整的DEMO:
"""""""""""""""""""""""""""""""""""""

    ::

        #-*-coding:utf-8-*-
        from app import app
        from flask.ext.login import LoginManager
        from flask import session, redirect, render_template, flash, request, url_for
        from forms import LoginForm
        from flask.ext.login import login_user,login_required, logout_user, UserMixin, AnonymousUser
        from user import User

        app = Flask(__name__)
        login_manager = LoginManager() #~產生一個Object
        login_manager.setup_app(app) #~基本配置

        SECRET_KEY = 'you-will-never-guess'

        #User class block
        #----------------------------------------------------
        class User(UserMixin):
            def __init__(self, name, id, active=True):
                self.name = name
                self.id = id
                self.active = active

            def is_active(self):
                return self.active

        #模擬USER DATABASE
        #----------------------------------------------------
        USERS = {
            1:User(u'Tomas', 1),
            2:User(u'Simon', 2),
            3:User(u'John', 3, False)
        }

        USER_NAMES = dict((u.name, u) for u in USERS.itervalues())
        #----------------------------------------------------

        @login_manager.user_loader
        def load_user(id):
            return USERS.get(int(id))  #回傳一個unicode ID 若是未登入則回傳None

        @app.route('/logout')
        @login_required #
        def logout():
            logout_user() #登出函式
            return redirect('/')
        @app.route('/')
        @app.route("/login", methods=["GET", "POST"])
        def login():
            form = LoginForm()
            if form.validate_on_submit():
                username = request.form['username']
                if username in USER_NAMES:
                    login_user(USER_NAMES[username]) #登入函式
                    return redirect(request.args.get('next') or url_for("index"))
                return redirect(url_for("login"))
            return render_template("login.html", form=form)
        @app.route('/index')
        @login_require
        def index():
            return " Login sucess! I'm index"

        @login_manager.unauthorized_handler
        def unauthorized():
            #收到錯誤後的動作
            a_response = 'login fail'
            return a_response

    登入錯誤處理：

    ::

        @login_manager.needs_refresh_handler
            def refresh():
                # do stuff
                return a_responses

    此範例登入錯誤時，request.args.get('next')會將頁面導向接下來的這一個函式，並且執行函式裡的動作。


Alternative Tokens
""""""""""""""""""""

    將資訊儲存在cookie中並不是一個安全的作法，較為安全的作法是結合帳號與密碼的驗證方式，你可以在User class中加入一個get_auth_token()，
    這個函式應該取得的是唯一識別碼，如同使用者的UID。

    同時在login_manager中也要設定一個token_loader的函式來與get_auth_token聯合運用。

    其中有另外一個function make_secure_token 可以建立一個auth_token ，這個token會連接所有的參數，並且用process的密鑰來確保資料的安全。

     (If you store the user’s token in the database permanently, then you may wish to add random data to the token to further
     impede guessing.)

    若是你的使用密碼來驗證，他舊有的auth_token失效。

Fresh Logins
""""""""""""""""""""""""""

    當使用者第一次登入時，session會紀錄狀態為fresh，但是當他登出重新登入，或是驗證毀損重新驗證後，session會紀錄該會員的狀態為'non-fresh'，
    再登入時不會有任何不同，有些時候的行為需要初次登入時進行，則可以利用這個狀態來做驗證判別。

    fresh_login_required除了驗證用戶登入也可以確保他們是初次登入，如果不是會送他們到另一個頁面。

    你也設定LoginManager.refresh_view 和 needs_refresh_message:

    login_manager.refresh_view = "accounts.reauthenticate"
    login_manager.needs_refresh_message = (u"To protect your account, please reauthenticate to access this page.")
    Or by providing your own callback to handle refreshing:

Session Protection
"""""""""""""""""""""""

    使用remember me 來記住登入資訊是不安全的，flask會協助進行安全性控管，保護你的資料外洩。

    你可以在LoginManager中設定保護模式，方法如下：

    login_manager.session_protection = "strong"

    Or, to disable it:

    login_manager.session_protection = None

    預設是basic模式。這個設定會再SESSION_PROTECTION為None, basic 或是 strong

    session 保護機制開啟的時候，每一個request都會有一個標示符號(基本上是用戶的IP和用戶代理的MD5值)，如果標示符相匹配，則該請求則允許。

3.Login session control
^^^^^^^^^^^^^^^^^^^^^^^^

login_user(user, remember=False, forcel=False)
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    .. code-block :: python

        def login_user(user, remember=False, force=False):
            '''
            Logs a user in. You should pass the actual user object to this. If the
            user's `is_active` method returns ``False``, they will not be logged in
            unless `force` is ``True``.

            This will return ``True`` if the log in attempt succeeds, and ``False`` if
            it fails (i.e. because the user is inactive).

            :param user: The user object to log in.
            :type user: object
            :param remember: Whether to remember the user after their session expires.
                Defaults to ``False``.
            :type remember: bool
            :param force: If the user is inactive, setting this to ``True`` will log
                them in regardless. Defaults to ``False``.
            :type force: bool
            '''
            if not force and not user.is_active():
                return False

            user_id = getattr(user, current_app.login_manager.id_attribute)()
            session['user_id'] = user_id
            session['_fresh'] = True
            session['_id'] = _create_identifier()

            if remember:
                session['remember'] = 'set'

            _request_ctx_stack.top.user = user
            user_logged_in.send(current_app._get_current_object(), user=_get_user())
            return True

    .. note ::

        session["user_id"] 紀錄user的id

        session["_fresh"]=True #紀錄該帳號是否為fresh

        session["_id"] = _create_identifier() (不知道是啥)

loginmanager._create_identifier()
""""""""""""""""""""""""""""""""""""

    創建一個獨有的標示符號

    .. code-block :: python

        def _create_identifier():
            user_agent = request.headers.get('User-Agent')
            if user_agent is not None:
                user_agent = user_agent.encode('utf-8')
                base = '{0}|{1}'.format(_get_remote_addr(), user_agent)
                if str is bytes:
                    base = unicode(base, 'utf-8', errors='replace')
                    h = md5()
                    h.update(base.encode('utf8'))
                    return h.hexdigest()

    .. note ::

        User_Agent ：request.headers.get('User-Agent')，取回brower中header中的User-Agent的值，
        另外也可以使用request.user_agent()。

        bytes 在python 中是一個不可變得資料型態，bytesarray是一個可變得序列。


    getattr example

    .. code-block :: python

        li=["tomas", "simon"]
        print li

        getattr(li, "append")("Moe")
        print li

        getattr(li, "pop")()
        print li

loginmanager.login_required
"""""""""""""""""""""""""""""

    python decorators method

    .. code-block :: python

        def login_required(func):
            '''
            If you decorate a view with this, it will ensure that the current user is
            logged in and authenticated before calling the actual view. (If they are
            not, it calls the :attr:`LoginManager.unauthorized` callback.) For
            example::

                @app.route('/post')
                @login_required
                def post():
                    pass

            If there are only certain times you need to require that your user is
            logged in, you can do so with::

                if not current_user.is_authenticated():
                    return current_app.login_manager.unauthorized()

            ...which is essentially the code that this function adds to your views.

            It can be convenient to globally turn off authentication when unit
            testing. To enable this, if either of the application
            configuration variables `LOGIN_DISABLED` or `TESTING` is set to
            `True`, this decorator will be ignored.

            :param func: The view function to decorate.
            :type func: function
            '''
            @wraps(func)
            def decorated_view( \*args, \*\*kwargs):
                if current_app.login_manager._login_disabled:
                    return func(\*args, \*\*kwargs)
                elif not current_user.is_authenticated():
                    return current_app.login_manager.unauthorized()
                return func(\*args, \*\*kwargs)
            return decorated_view

    .. note ::

        login_required 檢查是否登入的驗證decorators，驗證通過則顯示否則將會依照設定跳轉頁面。

        self._login_disabled = app.config.get('LOGIN_DISABLED',
                                                      app.config.get('TESTING', False))


loginmanager.fresh_login_required
"""""""""""""""""""""""""""""""""""

    驗證是否為初次登入

    .. code-block :: python

        def fresh_login_required(func):
            '''
            If you decorate a view with this, it will ensure that the current user's
            login is fresh - i.e. there session was not restored from a 'remember me'
            cookie. Sensitive operations, like changing a password or e-mail, should
            be protected with this, to impede the efforts of cookie thieves.

            If the user is not authenticated, :meth:`LoginManager.unauthorized` is
            called as normal. If they are authenticated, but their session is not
            fresh, it will call :meth:`LoginManager.needs_refresh` instead. (In that
            case, you will need to provide a :attr:`LoginManager.refresh_view`.)

            Behaves identically to the :func:`login_required` decorator with respect
            to configutation variables.

            :param func: The view function to decorate.
            :type func: function
            '''
            @wraps(func)
            def decorated_view(\*args, \*\*kwargs):
                if current_app.login_manager._login_disabled:
                    return func(\*args, \*\*kwargs)
                elif not current_user.is_authenticated():
                    return current_app.login_manager.unauthorized()
                elif not login_fresh():
                    return current_app.login_manager.needs_refresh()
                return func(\*args, \*\*kwargs)
            return decorated_view

    .. note ::

        login_fresh method 檢查session 的 _fresh 是否為 fresh。

        #: A proxy for the current user. If no user is logged in, this will be an
        #: anonymous user
        current_user = LocalProxy(lambda: _get_user() or current_app.login_manager.anonymous_user())

LoginManager.unauthorized()
""""""""""""""""""""""""""""

    user登入失敗時會自動導向到 'login_view'

    .. code-block :: python

        def unauthorized(self):
            '''
            This is called when the user is required to log in. If you register a
            callback with :meth:`LoginManager.unauthorized_handler`, then it will
            be called. Otherwise, it will take the following actions:

                - Flash :attr:`LoginManager.login_message` to the user.

                - Redirect the user to `login_view`. (The page they were attempting
                  to access will be passed in the ``next`` query string variable,
                  so you can redirect there if present instead of the homepage.)

            If :attr:`LoginManager.login_view` is not defined, then it will simply
            raise a HTTP 401 (Unauthorized) error instead.

            This should be returned from a view or before/after_request function,
            otherwise the redirect will have no effect.
            '''
            user_unauthorized.send(current_app._get_current_object())

            if self.unauthorized_callback:
                return self.unauthorized_callback()

            if not self.login_view:
                abort(401)

            if self.login_message:
                flash(self.login_message, category=self.login_message_category)

            return redirect(login_url(self.login_view, request.url))

    .. note ::

        若是'login_view'未宣告，則會送出 HTTP ERROR 401(Unauthorized)，可以導向next的變數


LoginManager.needs_refresh()
""""""""""""""""""""""""""""""

    當登入時需要身份為fresh，但該會員不是fresh時，將頁面自動導向'reefresh_view'頁面

    .. code-block :: python

        def needs_refresh(self):
            '''
            This is called when the user is logged in, but they need to be
            reauthenticated because their session is stale. If you register a
            callback with `needs_refresh_handler`, then it will be called.
            Otherwise, it will take the following actions:

                - Flash :attr:`LoginManager.needs_refresh_message` to the user.

                - Redirect the user to :attr:`LoginManager.refresh_view`. (The page
                  they were attempting to access will be passed in the ``next``
                  query string variable, so you can redirect there if present
                  instead of the homepage.)

            If :attr:`LoginManager.refresh_view` is not defined, then it will
            simply raise a HTTP 403 (Forbidden) error instead.

            This should be returned from a view or before/after_request function,
            otherwise the redirect will have no effect.
            '''
            user_needs_refresh.send(current_app._get_current_object())

            if self.needs_refresh_callback:
                return self.needs_refresh_callback()

            if not self.refresh_view:
                abort(403)

            flash(self.needs_refresh_message,
                  category=self.needs_refresh_message_category)

            return redirect(login_url(self.refresh_view, request.url))

    .. note ::

        若是'LoginManager.refresh_view'未宣告，頁面將會顯示HTTP ERROR 403



4.參考資料：
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

flask-login : https://flask-login.readthedocs.org/en/latest/

DEMO 參考：

.. 連結資訊：

.. _session : https://en.wikipedia.org/wiki/Session_(computer_science)

.. _cookie : https://en.wikipedia.org/wiki/HTTP_cookie


Flask Error Handler
-------------------------------------------

    400 錯誤的回傳訊息

    401 會員未登入

    403 權限不足

    404 無此網頁

    405 不允許此method

    406 不接受連線

    408 連線時間過長

    409 衝突錯誤

    410 網頁消失

    411 Length Required

    412 前提條件失敗

    413 Request 資料過大

    414 Url 資料過大

    415 不支援Media 類型

    416 請求範圍不符合

    429 Request 過多

    500 伺服器發生錯誤

    502 Gateway錯誤

    503 服務不可使用



Error Handlers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

        Error Handlers其實是一個view function，當error發生的時候會自動導向，
        顯示該頁面的資訊，

        demo:

        myapp.py

        ::

            from flask import Flask, render_template


            myapp = Flask(__name__)

            @myapp.route('/')
            def index():
                return 'index'

            @myapp.errorhandler(404)
            def page_not_found(e):
                return render_template('404.html'), 404

            myapp.run()

        layout.html

        ::

            <html>
            <head>
                {% block title %}{% endblock %}
            </head>
                <body>
                    {% block body %}{% endblock %}
                </body>
            </html>


        404.html

        ::

            {% extends "layout.html" %}
            {% block title %}Page Not Found{% endblock %}
            {% block body %}
              <h1>Page Not Found</h1>
              <p>What you were looking for is just not there.
              <p><a href="{{ url_for('index') }}">go somewhere nice</a>
            {% endblock %}

參考資料
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

        Flask Error Handlers:http://werkzeug.pocoo.org/docs/exceptions/#werkzeug.exceptions.HTTPException


flask-blueprint
----------------------------------------------

    每一組blueprint，都可以視為一張藍圖，blueprint 中有view 的集合，

    每一個URL對應一個View Class，或是一個View Function，而一個Application則是

    一個blueprint的集合。

	blueprint 提供模板，靜態文件...等等工具，blueprint 本身3並不是應用程式也沒有

	View的功能。

    DEMO

    blueprintdemo.py

    .. code-block  :: python

        from flask import Blueprint, render_template, abort
        from jinja2 import TemplateNotFound

        simple_page = Blueprint("simple page", __name__, template_folder='templates')

        @simple_page.route('/', defaults={"page": "index"})

        @simple_page.route('/<page>')
        def show(page):
            try:
                return render_template('pages/%s.html' % page)
            except TemplateNotFound:
                abort(404)

    app.py

    ..code-block :: python

        from flask import Flask
        from blueprintdemo import simple_page

        app = Flask(__name__)
        app.register_blueprint(simple_page)

        app.run()

Static File
^^^^^^^^^^^^^^^^^^^^^^^^^^^

    blueprint 可以指定static 資料夾，也可以選擇資料夾裡面的檔案利用url_for指定

    DEMO

    app.py

    ..code-block :: python

        from flask import Flask
        from blueprintdemo import simple_page

        app = Flask(__name__)

        admin = Blueprint('admin', __name__, static_folder='static')
        style = url_for('admin.static', filename='style.css')

        app.register_blueprint(simple_page)

        app.run()


Flask Basic info
---------------------------------------------------------

    Resquest 取得多個值(list)

        request.form.getlist('name')

    flask.abort

        Raises an HTTPException for the given status code.
        For example to abort request handling with a page not found exception,
        you would call abort(404).

    Demo

    .. code-block :: python

        from werkzeug.exceptions import HTTPException
        from flask import Flask, abort

        class PaymentRequired(HTTPException):
            code = 402
            description = '<p>You will pay for this!</p>'

        abort.mapping[402] = PaymentRequired

        app = Flask(__name__)
        @app.route('/')
        def mainpage():
            abort(402)

        @app.errorhandler(402)
        def payme(e):
            return 'Pay me!'

        app.run()




Flask-Admin
--------------------------------------
Introduction
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

	Flask-Admin利用簡單的class和method建立一個好用的後台模組。

	::

		class MyView(BaseView):
		@expose('/')
		def index(self):
			return self.render('admin/myindex.html')

		@expose('/test/')
		def test(self):
			return self.render('admin/test.html')

Quick start
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    要顯示的HTML儲存在templates/admin/內，Flask-Admin最重要的是model class以及
    和Database的溝通，可以依據個人需求對所需要的class做繼承的動作，而要擴展功能只要增加
    view就可以了。

Initialization
""""""""""""""""""""""""""""""""""""""

    ::

        from flask import Flask
        from flask.ext.admin import Admin

        app = Flask(__name__)

        admin = Admin(app)
        # Add administrative views here

        app.run()

    要連結到Flask-Admin的頁面網址為：http://localhost:5000/admin

    在上方會有一個Top-bar，你可以在Admin函數中設定左上角的顯示名稱。

    ::

        admin = Admin(app, name='Tomas')

    flask-admin不一定要用constructor產生物件，可以利用admin.init_app(app)進行
    應用程式的設定即可。

Adding views
""""""""""""""""""""""""""""""""""""""
        Flask-Admin可以利用class BaseView增加管理者頁面：

        ::

            from flask import Flask
            from flask.ext.admin import Admin, BaseView, expose

            class MyView(BaseView):
                @expose('/')
                def index(self):
                    return self.render('index.html')

            app = Flask(__name__)

            admin = Admin(app, name='Tomas')
            admin.add_view(MyView(name='Hello'))

            app.run()

        執行這個sample 你的Top-bar會多一個Hello的選項，每一個view都要有對應的html
        否則會無法作正確的顯示。

        index.html

        ::

            {% extends 'admin/master.html' %}
            {% block body %}
                Hello World from MyView!
            {% endblock %}

        master.html是Flask-Admin中就有預設好的html，所以在templates/admin中
        不需要有這個檔案，每個view的class 都要有@expose('/')這個預設的首頁否則會
        產生錯誤。

        Top-bar的選項可以設定為下拉式選單，demo為：

        ::

            from flask import Flask
            from flask.ext.admin import Admin, BaseView, expose

            class MyView(BaseView):
                @expose('/')
                def index(self):
                    return self.render('index.html')

                @expose('/test1/')
                def test1(self):
                    return self.render('test1.html')

                @expose('/test2/')
                def test2(self):
                    return self.render('test2.html')

                @expose('/test3/')
                def test3(self):
                    return self.render('test3.html')

            app = Flask(__name__)
            admin = Admin(app, name='Tomas')
            admin.add_view(MyView(name='Hello 1', endpoint='test1', category='Test')) #admin/test1
            admin.add_view(MyView(name='Hello 2', endpoint='test2', category='Test')) #admin/test2
            admin.add_view(MyView(name='Hello 3', endpoint='test3', category='Test')) #admin/test3

            app.run()

Authentication
"""""""""""""""""""""""""""""""""

    Flask-Admin並沒有任何關於帳號權限設定的功能，當你有這類的需求時可以配合Flask-login
    達到你的需求

    DEMO:

    ::

        class MyView(BaseView):
        def is_accessible(self):
            return login.current_user.is_authenticated()


Generating URLs
"""""""""""""""""""""""""""""""""

    跳轉頁面的時候可以利用url_for這個函式，在網址前方需要加上一個'.'，取得local的view：

    ::

        from flask import url_for

        class MyView(BaseView):
            @expose('/')
            def index(self)
                # Get URL for the test view method
                url = url_for('.test')
                return self.render('index.html', url=url)

            @expose('/test/')
            def test(self):
                return self.render('test.html')

    也可以跳到另一個view：

    ::

        class MyView(BaseView):
            @expose('/')
            def index(self)
                # Get URL for the test view method
                url = url_for('test2.test')
                return self.render('index.html', url=url)

File Admin
""""""""""""""""""""""""""""""""""

    可以產生一個資料夾內的檔案列表：

    ::

        from flask import Flask
        from flask.ext.admin.contrib.fileadmin import FileAdmin
        from flask.ext.admin import Admin

        import os.path as op

        app = Flask(__name__)
        DEBUG = True
        SECRET_KEY = 'Tomas Demo'
        app.config.from_object(__name__)
        admin = Admin(name='My App')
        path = op.join(op.dirname(__file__), 'static')
        admin.init_app(app)
        admin.add_view(FileAdmin(path, '/static/', name='static Files'))

        app.run()

    可以建立資料夾 ，創建檔案以及修改檔名，可以設定上傳檔案， 刪除檔案 或是資料夾等等權限設定，

    參考網站：http://flask-admin.readthedocs.org/en/latest/api/mod_contrib_fileadmin/#module-flask.ext.admin.contrib.fileadmin

    (有空再整理，加入TODO)



Working with templates
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    摘要：

    You can derive from template;(可以使用現有的模板)

    可以自己創建模板區塊

    可以自己決定是否切割模板

    可以自己建立相同名字的模板複寫原本的模板


    預設的大的模板為admin/master.html


    ============= ========================================================================
    head_meta     head 中的meta設定標籤
    title         頁面的名稱
    head_css      head 標籤中的css
    head          head標籤
    page_body     <body>標籤
    brand         Logo in the menu bar
    body          頁面會顯示的地方
    tail          Empty area below content
    ============= ========================================================================


PyMongo backend
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    Flask-Admin沒有檔案結構，這部份可以設定ModuleView達成。

    你在使用PyMongoDB的時候必須要注意的事項：

    設定colum_list顯示列表

    設定表單的屬性

    要先產生正確的PyMongo 物件。


Flask API
--------------

    Sample

    .. code-block:: python

        from flask import Flask
        from flask.ext.restful import Api, Resource

        app = Flask(__name__)
        api = Api(app)

        class UserAPI(Resource):
            def get(self, id):
                print id
                return 'aaaaa'
            def post(self, id):
                pass
            def delete(self, id):
                pass

        api.add_resource(UserAPI, '/users/<int:id>', endpoint = 'user')

        app.run()

    Validation RequestParser

    .. code-block:: python

        from flask import Flask, request, jsonify
        from flask.ext.restful import Api, Resource

        app = Flask(__name__)
        api = Api(app)

        @app.route('/todo/api/v1.0/tasks/<task_id>', methods = ['POST'])

        def update_task(task_id):

            task = request.get_json(force=True)

            if len(task) == 0:
                abort(404)
            if task is not request.json:
                abort(400)
            if 'title' not in request.json:
                abort(400)

            return jsonify( { 'task': task } )

        app.run(debug=True)


    similar way as arqparse

    .. code-block:: python


        #-*- coding:utf-8 -*-
        from flask import Flask, request, jsonify, abort
        from flask.ext.restful import Api, Resource, reqparse

        app = Flask(__name__)
        api = Api(app)

        class UserAPI(Resource):
            def get(self, id):
                print id
                return 'aaaaa'
            def post(self, id):
                pass
            def delete(self, id):
                pass

        class TaskListAPI(Resource):

            def __init__(self):

                self.reqparse = reqparse.RequestParser()
                self.reqparse.add_argument('title', type=str, required = True, help = u'No task title provided', location = 'json')
                self.reqparse.add_argument('description', type=str, default='', location='json')
                super(TaskListAPI, self).__init__()

            def post(self):

                task = request.get_json(force=True)
                #檢查request 的 args
                args = self.reqparse.parse_args()
                if len(task) == 0:

                    abort(404)

                if task is not request.json:

                    abort(400)

                return { 'task': task }, 202

        class TaskAPI(Resource):

            def __init__(self):

                self.reqparse = reqparse.RequestParser()
                self.reqparse.add_argument('title', type = str, location = 'json')
                self.reqparse.add_argument('description', type = str, location = 'json')
                self.reqparse.add_argument('done', type = bool, location = 'json')
                super(TaskAPI, self).__init__()

            def post(self):

                task = request.get_json(force=True)
                if len(task) == 0:

                    abort(404)

                if task is not request.json:

                    abort(400)

                if 'title' not in request.json:

                    abort(400)

                return jsonify( { 'task': task } )

            api.add_resource(UserAPI, '/users/<int:id>', endpoint = 'user')
            api.add_resource(TaskAPI, '/task/<int:id>', endpoint = 'task')
            api.add_resource(TaskListAPI, '/tasklist/', endpoint = 'tasklist')

            app.run(debug=True)

Request Parsing and Validation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^




Design Decisions in Flask
---------------------------

    1. The most important one is that implicit application objects require
        that there may only be one instance at the time. There are ways to
        fake multiple applications with a single application object, like
        maintaining a stack of applications, but this causes some problems
        I won’t outline here in detail.

        最重要的一點就是可以啟動一個APP object包含多個App的應用程序，但是這會有幾個問題。


專有名詞
^^^^^^^^^^^
* implicit application objects

參考文件
^^^^^^^^^^

    http://flask.pocoo.org/docs/design/


HTTP Basic Auth
-------------------------------

由Armin Ronacher提交，另有其他類似modules: `HTTP Digest Auth`_ , `Sign in with SteamID`_ , `Simple OpenID with Flask`_ 。

.. _HTTP Digest Auth : http://flask.pocoo.org/snippets/31/

.. _Sign in with SteamID : http://flask.pocoo.org/snippets/42/

.. _Simple OpenID with Flask : http://flask.pocoo.org/snippets/7/

1.簡介：
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

對於簡單的application來說，HTTP Basic Auth的登入機制已經足夠使用，

Flask要實作相當容易，下列所使用的decorator功能是針對特定的用戶。


2.Sample:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    **views.py**

    ::

        from flask import render_template, request, Response
        from functools import wraps
        from app import app
        def check_auth(username, password):
            """This function is called to check if a username /
            password combination is valid.
            """
            return username == 'admin' and password == 'secret'
        def authenticate():
            """Sends a 401 response that enables basic auth"""
            return Response(
            'Could not verify your access level for that URL.\n'
            'You have to login with proper credentials', 401,
            {'WWW-Authenticate': 'Basic realm="Login Required"'})
        def requires_auth(f):
            @wraps(f)
            def decorated(*args, **kwargs):
                auth = request.authorization
                print f(*args, **kwargs)
                if not auth or not check_auth(auth.username, auth.password):
                    return authenticate()
                return f(*args, **kwargs)
            return decorated
        @app.route('/')
        @app.route('/index')
        @requires_auth
        def index():
            return render_template('base.html')

    **run.py**

    ::

        from app import app
        app.run()

    **app/__init__.py**

    ::

        from flask import Flask
        app = Flask(__name__)
        from app import views

    **base.html**

    ::

        <html>
          <head>
            <title>Basic Auth Demo</title>
          </head>
          <body>
            Login success!!!
            </form>
          </body>
        </html>

3.Note:
^^^^^^^^^^^^^^^^^^^^^^^^^^^

warps : http://docs.python.org/dev/library/functools.html#functools.wraps

   .. note::

       warps可以協助你更便利的使用 partial(update_wrapper, wrapped=wrapped, assigned=assigned, updated=updated)

       定義一個decorator函式

Flask-RESTful
--------------

API
^^^^

    API : Application programing interface

Flask API
^^^^^^^^^^

    REST server 設計的六個重點

        * There should be a separation between the server that offers a service, and the client that consumes it.

        * 一個request 代表一個動作，也必須包含此動作所有的資料

        * API server需設定是否可以Cache

        * 在client 和 server 端中制定標準，結束後不論客端是否需要都需要回傳值

        * client 和 server 的方式必須是統一的

        * Server 視Client 需求提供script 或 lib 使用

Simple API
"""""""""""

    .. code-block:: python

        from flask import Flaskapp = Flask(__name__)

        @app.route('/')def index():

            return "Hello, World!"

        if __name__ == '__main__':

            app.run(debug=True)

    Demo

    .. code-block:: python

        from flask import Flask, jsonify

        app = Flask(__name__)

        tasks = [
            {

                'id': 1,
                'title': u'Buy groceries',
                'description': u'Milk, Cheese, Pizza, Fruit, Tylenol',
                'done': False

            },
            {

                'id': 2,
                'title': u'Learn Python',
                'description': u'Need to find a good Python tutorial on the web',
                'done': False

            }

        ]

        @app.route('/todo/api/v1.0/tasks', methods=['GET'])
        def get_tasks():

            return jsonify({'tasks': tasks})

        if __name__ == '__main__':

            app.run(debug=True)

Flask unitest
--------------

    Flask-unitest API

    .. code-block:: python

        def test_invalid_JSON(self):
        """Test status
        code 405 from improper JSON on post to raw"""
        response = self.app.post('/raw', data='{"aaa":"bbb"}',content_type='application/json')
        self.assertEqual(response.status_code, 405)


參考網頁
^^^^^^^^^

    API: http://zh.wikipedia.org/wiki/%E5%BA%94%E7%94%A8%E7%A8%8B%E5%BA%8F%E6%8E%A5%E5%8F%A3

    Flask API: http://flask.pocoo.org/docs/0.10/api/

    Flask API example: http://blog.miguelgrinberg.com/post/designing-a-restful-api-with-python-and-flask

    Flask-unitest: http://flask.pocoo.org/docs/0.10/testing/

    Flask-unitest API:http://hairycode.org/2014/01/18/api-testing-with-flask-post/

4.參考網站：
^^^^^^^^^^^^

HTTP Basic Auth : http://flask.pocoo.org/snippets/8/

flask-RESTful: http://blog.miguelgrinberg.com/post/designing-a-restful-api-using-flask-restful

**未解決問題：**

*request.authorization是什麼？*

*如何登出？*

**base.html**

