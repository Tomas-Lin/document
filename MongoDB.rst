.. sphinx documentation master file, created by
   sphinx-quickstart on Wed Apr 10 10:30:15 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


MongoDB
==================================

Geospatial Data and MongoDB
----------------------------

    MongoDB Version 2.4 之前

    ::

        {
            name: "Tomas",

            loc:[37.7577, -122.4376]

         }

    搜尋的方法有兩種

    定義一點與距離搜尋這半徑之內的所有資料

    * proximity:

        .. code-block:: javascript

            db.places.find({
                loc:{
                    '$near':[100, 100],
                    $maxDistance:10
                }
            });

    * Inclusion

    定義四點，組成一個長方形，在這個長方形內的所有資料

    ::

        db.places.find({
            loc:{
                $geoWithin:{
                    $polygon:
                        [0,0], [0,1], [1,1], [1,0]
                }
            }});

    MongoDB 2.4 之後改為使用 GeoJson_ 儲存地理資料

    GeoJson Sample

    .. code-block:: javascript


        name: "Tomas",

        loc{

            type: "point",

            coordinates: [35.7577, -122.4376]

        }

        // Polygon representation (Square) of a Food Truck Park
        {
            name: "Truckapalooza Square",

                loc : {

                    type : "Polygon",

                    coordinates : [ [ [ 0 , 0 ] , [ 0 , 1 ] , [ 1 , 1 ] , [  1 , 0 ] , [ 0 , 0 ] ] ]
            }
        }


    coordinates : 以陣列方式儲存紀錄經緯度

    type: Point, MultiPoint, LineString, MultiLineString,
          Polygon, MultiPolygon, Geometry Collection,
          Feature Object, Feature Collection Objects

    * Point: coordinates 紀錄只能有一個點

    * MultiPoint: 需為二維陣列紀錄多點

    * LineString: 兩點連成一線 所以陣列內需要有兩個點的紀錄

    * MultiLineString: 擁有多個點的array

    * polygon: 紀錄可以成為長方形的四點

    * MultiPolygon: 擁有多個點的array

    * Geometry Collection: 物件中的key值必須為 geometries 型態是陣列 裡面的元素都必須是GeoJson

    * Feature Object: type [Feature]

                      geometry [geometry object]

                      properties: [object]

    * Feature Objects Collections: The Collection of Feature

參考資料
^^^^^^^^^^^^^^^^^^^^^

Mongo Blog_

.. _Blog: http://blog.mongolab.com/2014/08/a-primer-on-geospatial-data-and-mongodb/

.. _GeoJson: http://geojson.org/

GridFS
---------

大綱
^^^^^^

    * GridFS 用來檢索或是儲存超過16MB的BSON-document

    * 什麼時候該使用 GridFS ？

        1. 如果你的檔案系統有限制目錄的檔案數量，你可以利用GridFS儲存文件

        2. 保持文件與數據跨多個系統和設備佈署

        3. 你希望可以閱讀大型文件內容而不需要完整的載入整份文件，GridFS可以協助呼叫部份的內容，
            不需要將整份文件載入記憶體

    * 不該使用GridFS

        1. 需要小部份的更新文件內容，替代翻案是可以儲存多個版本的，而指定最新的版本。

        2. 每次更新都必須更新全部內容的文件

    * 若你的文件是小於16MB的BSON，可以使用BinData數據類型來儲存二進制數據


GridFS Collections
^^^^^^^^^^^^^^^^^^^

    GridFS 儲存在兩個 collections

        * chunks 儲存binary chunks

            .. code-block :: json

                {
                    "_id" : <ObjectId>,
                    "files_id" : <ObjectID>,
                    "n" : <num>,
                    "data" : <binary>
                }

                _id : ObjectId

                files_id : 檔案的 ObjectId

                n : 序號，從0開始

                data : BSON的二進制資料

        * files 儲存檔案的metadata

            .. code-block :: json

                {
                    "_id" : <ObjectID>,
                    "length" : <num>,
                    "chunkSize" : <num>
                    "uploadDate" : <timestamp>
                    "md5" : <hash>

                    "filename" : <string>,
                    "contentType" : <string>,
                    "aliases" : <string array>,
                    "metadata" : <dataObject>,
                }

                _id : ObjectId

                length : 文件檔案大小

                chunksize : 區塊大小

                uploadDate : 初次儲存或是最後一次修改時間

                md5 : 從filemd5 API回傳的 MD5 hash 值

                filename : 檔案名稱

                contentType : MIME type

                aliases : 關鍵字陣列

                metadata : 備註

延伸閱讀
^^^^^^^^^^

    When should I use GridFS?

        http://docs.mongodb.org/manual/faq/developers/#faq-developers-when-to-use-gridfs

    Drive of BinData

        http://docs.mongodb.org/manual/applications/drivers/


MongoDB Indexing Strategies
---------------------------------

**作者:Tomas**


1. 如何有效的規劃資料庫的結構:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    1. 這個APP需要的資料為何?
    2. 每個欄位的搜尋頻率多寡
    3. 哪一個index在APP會有最多次機會被搜尋。

2. 規劃KEY值
^^^^^^^^^^^^^^^

    1. 在規劃MongoDB的index的Key值的時候，主要是一句你的query的搜尋條件key值有幾個，
    以減少query的次數為優先。

    2. MongoDB支援Compound indexes:複合索引 [#]_ ，在insert 、 find 、
    update 、remove(CRUD)都可以利用複合索引進行資料庫控制。

    3. 可以在搜尋同時設定你所需要回傳的資料，如下例

::

    # db.test.insert({ 'user': 'Thomas'})

    #db.test.find({ 'user' : 'Thomas'}, {'user' : 1, '_id' : 0})



    4. 若是值的型態是array(object)，則該筆資料並不支援cover query。
    5. 建立一個indexes的 result再利用sort()來控制Covered Query，如下例:

::

    #db.test.insert({ a: 1, b: 1, c: 1, d: 1 })

    #db.collection.find().sort( { a:1 } )
    #db.collection.find().sort( { a:1, b:1 } )

    #db.collection.find( { a:4 } ).sort( { a:1, b:1 } )
    #db.collection.find( { b:5 } ).sort( { a:1, b:1 } )

    #db.collection.find( { a:5 } ).sort( { b:1, c:1 } )

    #db.collection.find( { a:5, c:4, b:3 } ).sort( { d:1 } )
    #db.collection.find( { a: { $gt:4 } } ).sort( { a:1, b:1 } )
    #db.collection.find( { a: { $gt:5 } } ).sort( { a:1, b:1 } )
    #db.collection.find( { a:5, b:3, d:{ $gt:4 } } ).sort( { c:1 } )
    #db.collection.find( { a:5, b:3, c:{ $lt:2 }, d:{ $gt:4 } } ).sort( { c:1 } )

\
    6. `db.collection.totalIndexSize() <http://docs.mongodb.org/manual/reference/method/db.collection.totalIndexSize/#db.collection.totalIndexSize>`_ :可以利用此指令查詢此collection所佔的byte大小

    7. Ensure Indexes Fit RAM

::

    > db.collection.totalIndexSize()
    4294976499

在我執行MongoDB的指令時必須確保有足夠的暫時記憶體可以存放資料，
上述範例中做的搜尋有4.3G的大小，若是剩餘的佔存記憶體不夠，則會占用
一般記憶體，則會影響到效率，要多加注意。而有一部分的indexes原本就存放
在佔存記憶體中，則不需要計算在內。

See also
For additional.collection statistics [#]_ , use.collStats or db.collection.stats(). [#]_

3. 利用搜尋條件來過濾掉不必要的資料，增加回傳資料的準確性。
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. 若是你的APP是一個重度寫入資料庫的模式，更需要注意如何設計indexes，否則對於效率會有極大的影響。

2. 一個不常使用的index，將會影響搜尋或是修改的效率。

3. 若collection的資料是有大量的indexes與重度寫入的狀態，你必須要社記一個可以減少執行query次數的資料架構。

4. 你在搜尋的條件設定會影響效率問題。



4.  參考資料:
^^^^^^^^^^^^^^^^^^^

http://docs.mongodb.org/manual/applications/indexes/

5. 備註資料:
^^^^^^^^^^^^^^^

.. [#] : `Compound indexes:複合索引 <http://docs.mongodb.org/manual/core/indexes/#index-type-compound>`_
.. [#] : `.collection statistics <http://docs.mongodb.org/manual/reference/collection-statistics/>`_
.. [#] :  `collStats <http://docs.mongodb.org/manual/reference/command/collStats/#dbcmd.collStats>`_ , `db.collection.stats() <http://docs.mongodb.org/manual/reference/method/db.collection.stats/#db.collection.stats>`_

........................................................................

Mongodb-pymongo初階教學
-----------------------

`pymongo <http://api.mongodb.org/python/current/>`_ 是一個


**作者:Tomas**

1.install （安裝）
^^^^^^^^^^^^^^^^^^

::

    $sudo yum install mongodb libmongodb mongodb-server

2.start（開始）
"""""""""""""""""

::

    $sudo service mongod start
    $chkconfig mongod on

3.test_pymongo.py（測試）:
""""""""""""""""""""""""""""

.. code-block :: python

    # -*- coding: utf-8 -*-
    from pymongo import Connection
    from time import time, strftime, localtime
    from random import randint
    class PyConn(object):

        def __init__(self, host, port):
            self.dbname=''
            self.collname=''
            try:
                self.conn = Connection(host, port)
            except  Error:
                print 'connect to %s:%s fail' %(host, port)
                exit(0)

        def __del__(self):
            self.conn.close()

        def setDB(self, dbname):
            # 這種[]獲取方式同样适用於shell,下面的collection也一样
            #db 類型<class 'pymongo.database.Database'>
            self.db = self.conn[dbname]
            self.dbname=dbname

        def setCollection(self, collection):
            if not self.db:
                print 'don\'t assign database'
                exit(0)
            else:
                self.coll = self.db[collection]
                self.collname=collection

        def find(self, \*\*query): #查詢
            try:
                #result類型<class 'pymongo.cursor.Cursor'>
                if not self.coll:
                    print 'don\'t assign collection'
                else:
                    result = self.coll.find(query)
            except NameError:
                print 'some fields name are wrong in ',query
                exit(0)
            return result

        def find_one(self, \*\*query): #查詢
            print query
            try:
                #result類型<class 'pymongo.cursor.Cursor'>
                if not self.coll:
                    print 'don\'t assign collection'
                else:
                    result = self.coll.find_one(query)
                    print result
            except NameError:
                print 'some fields name are wrong in ',query
                exit(0)
            return result

        def insert(self, \*\*data):   #新增
            #insert會返回新插入數據的_id
            self.coll.insert(data)

        def remove(self, \*\*data): #刪除
            self.coll.remove(data)

        def update(self, data, \*\*setdata): #修改
            if type(data) is not dict or type(setdata) is not dict:
                print 'the type of update and data isn\'t dict'
                exit(0)
            #update無返回值
            self.coll.update(data,{'$set':setdata})

執行測試:

::

    $python test_pymongo.py

connection pooling
"""""""""""""""""""

    connection chrun: 每一個request 都開啟一個新的connection 與 close，造成過於頻繁的連線建立與關閉稱之為connection chrun，但是若是開啟過多的connection 但是不關閉的話，則會造成記憶體的負擔從而造成performace不佳。

    MongoDB 的Modules常常都會有使用MongoClient來建立一個connection，他的回傳值是一個object，一個application在initialize的時候就可以asign 一個 MongoDB connection，

    example: db = MongoClient(app)

    而在這個application 中都使用這個 db object進行資料庫的操作，最常遇到的一個問題就是產生太多的 MongoClient Object，造成過於頻繁的建立新的連線。

參考資料
""""""""""""""

    http://rritw.com/a/shujuku/20120626/176249.html

    connection pooling: http://blog.mongolab.com/2013/11/deep-dive-into-connection-pooling/

    MongoDB : http://docs.mongodb.org/meta-driver/latest/legacy/notes-on-pooling-for-mongodb-drivers/

MongoDB Applied Design Patterns
--------------------------------------------------------


MongoDB Manual
-----------------

Indexes
^^^^^^^^^^

    Index 儲存於**記憶體**，可以增加搜尋效率。

    MongoDB 提供多種儲存Indexes的方式

        * Default _id

            儲存MongoDB的預設 _id值

        * single field

            MongoDB支援user 自定義的Indexes

        * compound Index(複和索引)

            MongoDB 支援user 自定義的多重Index，類似single-field，最多支援31個欄位的複和式索引

            複合式索引不能夠使用hashed field，如果你的複合式索引內有一個是hashed
            field無法執行並且會收到一個錯誤

補充
^^^^^^^^

    hashed index :

        新增於Version 2.4，不支援multi-key field支援sharding 叫作hashed shard key，
        在sharding中使用hashed shard key*可以更均勻的分散數據*，不支援range queries
        **however, you can create both a hashed index and an ascending/descending
        (i.e. non-hashed) index on the same field:I MongoDB will use the scalar index
        for range queries**

        .. note ::

            64 bit的系統會自動將float轉為int 再做hash的動作，為了防止衝突在64bit的系統不要
            設定float 而32 bit的hash 最多到小數點後第2位


2Mongodb
----------------

.. toctree::

MongoDB教學
-----------------

翻譯者: Tomas、卓青

原文連結:
`在此 <http://www.hacksparrow.com/the-mongodb-tutorial.html>`_

使用文件之前請先安裝MongoDB。
連結:


安裝好後啟動MongoDB server

::

    $ mongod

啟動MongoDB Client 使用mongo client連線到MongoDB server:

::

    $ mongo

**注意:本文件的所有MongoDB 指令在Client 端都在 “>” 此符號後輸入。**

Getting acquainted with MongoDB
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


MongoDB 是一個
:command:`文件導向式`
的資料庫系統(一種NoSQL資料庫)，MongoDB 的結構依序是

資料庫(database)>
:command:`集合(collection)`
>文件(document)>
:command:`區域(field)`
> 索引:值(key:value)。


.. note::

    kevin's note：
    集合(collection)是指BSON( “BSON” is a portmanteau of the words “binary” and “JSON”)
    documents的分組方式，Collections不是強制性的架構，有點類似RDBMS（Relational Database Management System）的tables，
    可以有很多不同的documents在一個collection的範圍內，但在一個相同的collection內的documents通常都會有類似或有相關聯目的的應用。

    `資料來源 <http://docs.mongodb.org/manual/reference/glossary/#term-collection>`_

    區域(field)就是
    `長這個樣子 <http://docs.mongodb.org/manual/core/document/#structure>`_



下方為MongoDB 與 SQL資料模型的比較圖：

============ ============
MongoDB      SQL
------------ ------------
database     database
collection   table
document     row
field        -
{key:value}  -
============ ============


**要熟悉記住上方的對應表**, 因為接下來都是用MongoDB的用語來說明，不會再出現像是
:command:`row, table`
這些SQL資料庫會出現的術語。

要熟悉MongoDB術語~~!!

MongoDB shell是一個完整的 **javascript shell** ，你可以在在MongoDB shell **直接執行**
javascript對資料的運算或是與MongoDB指令配合使用寫入資料庫。

MongoDB shell 支援所有標準的javascript API ，這是MongoDB重要的特色之一。

MongoDB shell 是一個提供全功能的JavaScript shell , 你可以在加入或檢索查詢資料之前,執
行對資料的JavaScript運算。

MongoDB shell支援所有標準JavaScript API ,以及包含Mongo
所獨有的客製化API, 這些特色讓MongoDB成為非常強大的資料庫系統。

在你 **整理資料庫** 中的資料之前，你必須
:command:`先了解 如何分配資料庫內的資料` ，這是準備開始與
MongoDB互動的基礎能力。

在開始 **操作資料庫** 資料之前，你必須
:command:`先了解如何跟資料庫系統做溝通`， 這是你跟 MongoDB互動的基礎。

查詢在系統中的資料庫列表:

::

    > show dbs
    bands 0.203125GB
    movies (empty)
    scores (empty)

跟使用其他SQL資料庫一樣(例如MySQL), 你首先選擇要使用來執行資料運算的資料庫：

::

    > use movies
    switched to db movies

顯示目前正在使用的資料庫:

::

    > db.getName()
    movies

顯示所有的資料庫

::

    > show dbs

刪除(delete / drop )你正在使用的資料庫: ( **刪除前需確定你選擇的資料庫是否正確。** )

::

    > use scores
    > db.dropDatabase()
    {"dropped" : "scores", "ok" : 1 }

顯示所選擇的資料庫的所有集合(collections)，下列兩種方法皆可:

::

    > show collections

或是

::

    > db.getCollectionNames()

不像 `SQL <https://zh.wikipedia.org/wiki/SQL>`_ 型態的資料庫,
MongoDB建立database是很被動的, 並不需要使用者明確的下命
令來建立資料庫 ( 例如MySQL資料庫要下create database指令才會建立資料庫, 但是
MongoDB 不用 )。
MongoDB會依照情況確定要建立資料庫的時候才會自動建立。

MongoDB建立資料庫是很方便的，你不需要輸入建立資料庫的指令，當你寫入第一筆資料
的時候，MongoDB會自動幫你建立資料庫。

要建立一個新資料庫, 首先要使用use命令切換到新的資料庫, 然後執行儲存一些資料到資料
庫中, 這個時候MongoDB就會幫你建立資料庫了。

對movies寫入第一筆資料，MongoDB自動建立movies資料庫並且將資料寫入資料庫內。

我們建立一個新的資料庫叫作 movies:

::

    > use movies
    switched to db movies
    > db.comedy.insert({name:"Bill & Ted's Excellent Adventure",
    year:1989})

:command:`建立collection跟建立資料庫的方式一樣，寫入第一筆資料後會自動建立一個collection，
寫入資料指令中的comedy是collection名稱。`

上面命令會產生建立一個叫作movies的資料庫, 在這過程中, 一個叫作comedy的collection
被建立到資料庫中。

Getting your hands dirty with data in MongoDB
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

這邊用範例來說明MongoDB 如何CRUD(建立、讀取、修改、刪除)資料。

注意所有MongoDB指令都是使用MongoDB資料庫物件db的method, db所參照的資料庫
就是你使用use指令所選擇的資料庫。

Create / add data in MongoDB
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

在collection裏面建立一些documents(文件):

::

    > db.comedy.insert({name:"Wayne's World", year:1992})

::

    > db.comedy.insert({name:'The School of Rock', year:2003})

上述兩個指令對名稱為comedy的collection寫入兩筆資料。

save()與insert()結果都是一樣寫入資料，使用方法也一致。

::

    > db.comedy.save({name:"Wayne's World", year:1992})
    > db.comedy.save({name:'The School of Rock', year:2003})

insert和save的差別在於:

insert新增一筆新的docuement;

save是如果這個沒有符合這個物件的_id key值就新增, 若有_id key存在則update(更新)在資
料庫符合_id鍵值的資料。

**如果非必要最好是使用insert()新增資料，update()修改資料。**
**建議使用insert新增documents, 用update命令更新在collection裏面的docuement資料。**

Read data from MongoDB
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

從collection裏面讀資料: (讀取comdy裡的資料)

::

    > db.comedy.find()
    { "_id" : ObjectId("4e9ebb318c02b838880ef412"), "name" : "Bill &
    Ted's Excellent Adventure", "year" : 1989 }
    { "_id" : ObjectId("4e9ebb478c02b838880ef413"), "name" : "Wayne's
    World", "year" : 1992 }
    { "_id" : ObjectId("4e9ebd5d8c02b838880ef414"), "name" : "The
    School of Rock", "year" : 2003 }

讀取collection資料後用limit()限定顯示資料筆數2筆:

::

    > db.comedy.find().limit(2)
    { "_id" : ObjectId("4e9ebb318c02b838880ef412"), "name" : "Bill &
    Ted's Excellent Adventure", "year" : 1989 }
    { "_id" : ObjectId("4e9ebb478c02b838880ef413"), "name" : "Wayne's
    World", "year" : 1992 }

跟find().limit(1)相似的指令是findOne(), 只會傳回collection中的一筆document。

::

    db.comedy.findOne()
    {
    "_id" : ObjectId("4e9ebb318c02b838880ef412"),
    "name" : "Bill & Ted's Excellent Adventure",
    "year" : 1989
    }

附有條件的搜尋範例如下:

::

    > db.comedy.find({year:{$lt:1994}})
    { "_id" : ObjectId("4e9ebb318c02b838880ef412"), "name" : "Bill &
    Ted's Excellent Adventure", "year" : 1989 }
    { "_id" : ObjectId("4e9ebb478c02b838880ef413"), "name" : "Wayne's
    World", "year" : 1992 }

上面範例我們搜尋的條件是在year這個field(欄位)中值小於1994的,
$lt是MongoDB中的條件選項的其中一個，下方列出其他條件式運算:

::

    $lt - ' $lte - '
    $gte - '>='
    $ne - '!='
    $in - 'is in array'
    $nin - '! in array'

我們要如何執行'equal to' (==) 查詢? 下述的範例是搜尋year **符合1992** 的資料:

::

    > db.comedy.find({year:1992})
    { "_id" : ObjectId("4e9ebb478c02b838880ef413"), "name" : "Wayne's
    World", "year" : 1992 }

我們可以使用 regular expressions(正規運算式)查詢資料:

::

    > db.comedy.find({name:{$regex: /bill|steve/i}})
    { "_id" : ObjectId("4e9ebb318c02b838880ef412"), "name" : "Bill &
    Ted's Excellent Adventure" }

嘗試進階的regex搜索方式:

::

    > var names = ['bill', 'steve']
    > names = names.join('|');
    > var re = new RegExp(names, 'i')
    > db.comedy.find({name:{$regex: re}})
    { "_id" : ObjectId("4e9ebb318c02b838880ef412"), "name" : "Bill &
    Ted's Excellent Adventure" }

如果我只要取得結果的某些欄位就好:

::

    > db.comedy.find({year:{'$lt':1994}}, {name:true})
    { "_id" : ObjectId("4e9ebb318c02b838880ef412"), "name" : "Bill & Ted's Excellent Adventure" }
    { "_id" : ObjectId("4e9ebb478c02b838880ef413"), "name" : "Wayne's World" }
    > db.comedy.find({year:{'$lt':1994}}, {name:true})
    { "_id" : ObjectId("4e9ebb318c02b838880ef412"), "name" : "Bill & Ted's Excellent Adventure" }
    { "_id" : ObjectId("4e9ebb478c02b838880ef413"), "name" : "Wayne's World" }

上面範例會搜尋出comedy colletion 中year欄位中所有小於1994的資料, 只顯示符合資料
的name欄位。

像是 db.comedy.find({year:{'$lt':1994}}, {name:true, director:true}) 會搜尋出comedy
colletion 中year欄位中所有小於1994並且只顯示name以及director欄位的資料。

:command:`注意: 但是因為資料庫裡面沒有director欄位的資料，所以回傳的資料即使設定true也沒有director的資料。`

_id欄位為 **預設一定會回傳的欄位** ，若要隱藏則在條件後需要增加{_id:false}隱藏_id欄位

::

    > db.comedy.find({year:{'$lt':1994}}, {name:false})
    { "_id" : ObjectId("4ed201525bfc796ab22f9ee1"), "year" : 1989 }
    { "_id" : ObjectId("4ed201605bfc796ab22f9ee2"), "year" : 1992 }

MongDB
:command:`不能同時` 使用欄位的inclusion(列入) 和 exclusion(排除)運算, 例如你不能寫說我只
能顯示name欄位但是不要顯示year欄位, 你只能寫我要顯示name欄位, 或是我不要顯示
year欄位, 選一個。

MongoDB獨有的查詢語言(你可以參考 `dot notation <http://docs.mongodb.org/manual/>`_  。)


開一個新的collection 名稱為articles並寫入資料：

::

    > db.articles.insert({title:'The Amazingness of MongoDB', meta:
    {author:'Mike Vallely', date:1321958582668, likes:23, tags:
    ['mongo', 'amazing', 'mongodb']}, comments:[{by:'Steve',
    text:'Amazing article'}, {by:'Dave', text:'Thanks a ton!'}]})
    > db.articles.insert({title:'Mongo Business', meta:{author:'Chad
    Muska', date:1321958576231, likes:10, tags:['mongodb', 'business',
    'mongo']}, comments:[{by:'Taylor', text:'First!'}, {by:'Rob',
    text:'I like it'}]})
    > db.articles.insert({title:'MongoDB in Mongolia', meta:
    {author:'Ghenghiz Khan', date:1321958598538, likes:75, tags:
    ['mongo', 'mongolia', 'ghenghiz']}, comments:[{by:'Alex',
    text:'Dude, it rocks'}, {by:'Steve', text:'The best article
    ever!'}]})

要搜尋物件裏面的物件, 只要使用標準的JavaScript dot notation (點運算子)就可以存取物件
底下的任何一個子物件。
例如, 要查詢 meta.likes 物件, 你的作法如下:

::

    > db.articles.find({'meta.author':'Chad Muska'});
    > db.articles.find({'meta.likes':{$gt:10}})

**查詢陣列** 作法如下:

::

    > db.articles.find({'meta.tags':'mongolia'})

若是你要搜尋陣列內包含的物件，搜尋方式如下：

::

    > db.articles.find({'comments.by':'Steve'})

你可以指定要找陣列的哪個index(索引)的資料：

::

    > db.articles.find({'comments.0.by':'Steve'})

使用dot notation(點運算子)來陣列內物件的時候, 會有機會找到有相同key,但是不同value
的情況發生, 這個時候我們需要使用$elemMatch運算子。

補充資料：

$elemMatch

::

    // Document 1
    { "foo" : [
        {
            "shape" : "square",
            "color" : "purple",
            "thick" : false
        },
        {
            "shape" : "circle",
            "color" : "red",
            "thick" : true
        }
    ]}

    // Document 2
    { "foo" : [
        {
            "shape" : "square",
            "color" : "red",
            "thick" : true
        },
        {
            "shape" : "circle",
            "color" : "purple",
            "thick" : false
        }
    ] }

::

    > db.foo.find({"foo.shape": "square", "foo.color": "purple"});

若輸入上方指令由於資料內皆有符合條件，所以兩筆資料都會被顯示，這時候我們就需要
使用到$elemMatch 這個變數，範例如下：

::

    db.foo.find({foo: {"$elemMatch": {shape: "square", color: "purple"}}});

上述指令只會搜尋第一個物件中符合條件的資料顯示。

注意如果把數字用雙引號括起來, 那數字就是字串, 下面兩種查詢並不相同:

::

    > db.students.find({score:100})

::

    > db.students.find({score:'100'})

回傳資料型態均為字串, 因此如果你要使用的是數值, 那記得都要做轉換。

例如在Express.js中, 你可以這樣做:

::

    var score = +params.score;

params.score是一個 **字串** , 但是經由這樣的運算式, 變數score就是一個實際的JavaScript
number型態的資料, 可以在MonogoDB查詢中被可靠的使用。


MongoDB 可以支援javascript 運算式，範例如下：

::

    > db.comedy.find('this.year > 1990 && this.name != "The School of Rock"')

等同於使用MongoDB query operators:

    > db.comedy.find({year:{$gt:1990}, name:{$ne:'The School of Rock'}})

:command:`在mongoDB中使用JavaScript運算式比直接用原生的MongoDB operators 效率差。`

MongoDB有另外的運算子$where 可以使用，讓你可以使用像是SQL使用的WHERE方式,
使用範例如下：

::

    > db.comedy.find({$where: 'this.year > 2000'})
    { "_id" : ObjectId("4ed45fae2274c776f9179f89"), "name" : "The
    School of Rock", "year" : 2003 }

以及:

::

    > db.comedy.find({name:'The School of Rock', $where: 'this.year >
    2000'})
    { "_id" : ObjectId("4ed45fae2274c776f9179f89"), "name" : "The School of Rock", "year" : 2003 }

使用$where效率 **低於** 使用MongoDB運算子的搜尋效率, 所以除非使用MongoDB原生運算
子無法達到你搜尋需求的時候, 才使用JavaScript運算式或是$where。

查閱 `advanced MongoDB query operators <http://docs.mongodb.org/manual/reference/operators/>`_  頁面取得更多使用MongoDB運算子的使用方式。

Update data in MongoDB
^^^^^^^^^^^^^^^^^^^^^^^

我們嘗試在documents中加入新的field(欄位)：

::

    > db.comedy.update({name:"Bill & Ted's Excellent Adventure"},
    {'$set':{director:'Stephen Herek', cast:['Keanu Reeves', 'Alex
    Winter']}})

如何更新陣列中的值, 方法如下:

::

    > db.comedy.update({name:"Bill & Ted's Excellent Adventure"},
    {'$push':{cast:'George Carlin'}})
    > db.comedy.update({name:"Bill & Ted's Excellent Adventure"},
    {'$push':{cast:'Chuck Norris'}})

移除陣列中的資料方法如下:

::

    db.comedy.update({name:"Bill & Ted's Excellent Adventure"},
    {'$pull':{cast:'Chuck Norris'}})

更多update細節可以參考: `這裡 <http://docs.mongodb.org/manual/applications/update/>`_ 。


Delete data in MongoDB
^^^^^^^^^^^^^^^^^^^^^^^

從一個document中刪除一個欄位:

::

    > db.comedy.update({name:'Hulk'}, {$unset:{cast:1}})

所有collection裏面的document, 都要刪除這樣的一個欄位:

::

    > db.comedy.update({$unset:{cast:1}}, false, true)

false參數是給upsert選項用
:command:`( upsert是? )` , true參數是給
:command:`multiple選項` 用。
我們把multiple選項設為true, 表示我們要刪除collection裏面的所有符合條件的document項目。
不過這個用法不太妥當, 應該要使用 find()或是 findOne()選擇來執行update。

::

    > db.comedy.remove({name:'Hulk'})

上面命令會刪除所有name值是Hulk的所有documents。

清空collection中的所有documents資料：

::

    > db.comedy.remove()

上述命令就像是SQL的truncate命令, 沒有刪除collection, 而是reset這個collection。

delete/drop 一個collection :

::

    > db.comedy.drop()


刪除資料庫如下例：

先選擇database ,然後呼叫db.dropDatabase()。

::

    > use movies
    > db.dropDatabase()
    {"dropped" : "movies", "ok" : 1 }

count() 此函數可以使用在計算collection中的 **資料筆數** ：

::

    > db.comedy.count({})

也可以計算collection中 **特定條件的資料筆數** ，範例如下：

::

    > db.comedy.count({year:{$gt:1990})


進階參考:

MongoDB Documentation

1. `MongoDB Manua l <http://docs.mongodb.org/manual/contents/>`_
2. `Nightly generated PDF of the MongoDB Manua l <http://dl.mongodb.org/dl/docs/>`_

Further Reading

1. `MongoDB Introduction <http://www.mongodb.org/about/introduction/>`_
2. `SQL to Mongo Mapping Chart <http://docs.mongodb.org/manual/reference/sql-comparison/>`_
3. `MongoDB Advanced Queries <http://docs.mongodb.org/manual/reference/operators/>`_
4. `MongoDB Updating <http://docs.mongodb.org/manual/applications/update/>`_
5. `MongoDB Aggregation <http://docs.mongodb.org/manual/aggregation/>`_
6. `MongoDB Schema Design <http://docs.mongodb.org/manual/core/data-modeling/>`_
7. `MongoDB Indexes <http://docs.mongodb.org/manual/core/data-modeling/>`_
8. `MongoDB Cookbook <http://cookbook.mongodb.org/>`_
9. `MongoDB JavaScript API <http://api.mongodb.org/js/>`_

MongoDB Document
-----------------------------

MongoDB CRUD
-------------------
Introduction
^^^^^^^^^^^^^^

    MongoDB 利用BSON的方式儲存資料，每一筆資料中的欄位都是key 與 value的配對。

    Collections 是資料的集合，類似關聯似資料庫的table

    Query 提供條件進行操作，而query所給予的option 可以控制回傳的資料

    Data Modification 就是資料庫的CRUD

CRUD Concepts
^^^^^^^^^^^^^^^^

Read Operations Overview
""""""""""""""""""""""""""

    Query 大分為三個部份

        * query criteria

        * projection

        * cursor modifiler

    Query Behavior

        * 一次Query只對一個Collection做資料處理

        * 可以利用query對搜尋的資料做整理，限制與排序

        * 除非使用sort()，否則MongoDB不會自動幫你整理排序

        * 修改的Query跟先搜尋在修改資料的Query是一樣的

        * **In aggregation pipeline, the $match pipeline stage provides access to MongoDB queries.**

    Projection

        * 預設回傳所有吻合的資料

        * 利用projection 的搜尋結果回傳給client，可以減少網路流浪與程式負載

        * Except for excluding the _id field in inclusive projections, you cannot mix exclusive and inclusive projections.

        **exclusive？？inclusive？？**

    Cursor

        * 在MongoDB shell 中，20筆資料

        * 一個記憶體位置有一個指標指向一筆資料。

        ＊ 手動移動指標搜尋資料的方法請參照：
        **延伸未讀http://docs.mongodb.org/master/tutorial/iterate-a-cursor/**

        * Cursor Isolation:搜尋的時候cursor並不是獨立的，所以有可能在同時document被修改後，回傳的document不只一筆，要預防這種狀況請參照：
          延伸未讀 **http://docs.mongodb.org/master/faq/developers/#faq-developers-isolate-cursors**

        * 大部份第一次的 batches 大約會回傳101筆document，至少會佔1MB~4MB的記憶體，
          Batches size 的大小 : 延伸未讀 **http://docs.mongodb.org/master/reference/limits/#limit-bson-document-size**
          要修改Batches 請參照：
          延伸未讀 **http://docs.mongodb.org/master/reference/method/cursor.batchSize/#cursor.batchSize**
          延伸未讀 **http://docs.mongodb.org/master/reference/method/cursor.limit/#cursor.limit**

        * 若沒有設定index ，MongoDB必須將所有的document載入記憶體中，並且將所有的document 第1筆的 batches 回傳所有docuement

        * objsLeftInBatch()

          .. code-block :: javascript

            var myCursor = db.inventory.find();

            var myFirstDocument = myCursor.hasNext() ? myCursor.next() : null;

            myCursor.objsLeftInBatch();

        * MongoDB shell 中可以執行 db.runCommand( { cursorInfo: 1 } ) 得到下列幾項資訊

            * total number of open cursors(WTF？)

            * client 中 cursor 的大小

            * 啟動服務後 有幾次Timeout error

Query Optimezation
""""""""""""""""""""

    * Index 可以提高讀取資料效率，並且簡化MongoDB queries

    * Index 可以針對單個或多個欄位，若搜尋的欄位有Index的欄位，可以避免搜尋整個document
      延伸未讀 ： **http://docs.mongodb.org/master/core/indexes/**

      ..code-block :: javascript

        Mongo shell:

        db.inventory.ensureIndex( { type: 1 } )

        Example
        var typeValue = <someUserInput>;
        db.inventory.find( { type: typeValue } );

    * 分析Query performance :
      延伸未讀L **http://docs.mongodb.org/master/tutorial/analyze-query-plan/**

    * '$nin', '$ne'不管有沒有設定 index, 對於效率不會有太多的改善，而regular expressions($regex)不能使用index，
      **Queries that specify regular expression with anchors at the beginning of a string can use an index.**

    * Query中所有的field 是都包含在index的fiedl，並且回傳的所有field 都是index 裏面的field
      延伸未讀： **http://docs.mongodb.org/master/tutorial/create-indexes-to-support-queries/#indexes-covered-queries**

Query Plans
""""""""""""""

    * 可以利用 method  **explain()** 來觀察query plans 並且改進你的 indexing strategies.延伸未讀 **http://docs.mongodb.org/master/applications/indexes/**

    * 建立 Query 最佳化

        * 針對要最佳化的index 同時進行多次的query

        * 紀錄每次query 的 buffer 或 buffers

            * If the candidate plans include only ordered query plans, there is a single common results buffer.

            * If the candidate plans include only unordered query plans, there is a single common results buffer.

            * If the candidate plans include both ordered query plans and unordered query plans, there are two common results buffers, one for the ordered plans and the other for the unordered plans.

        * 停止unitest 並且檢查每一個 Query 的buffer 或　buffers

            * An unordered query plan has returned all the matching results; or

            * An ordered query plan has returned all the matching results; or

            * An ordered query plan has returned a threshold number of matching results:

        * 延伸未讀：
          **http://docs.mongodb.org/master/reference/glossary/#term-unordered-query-plan**
          **http://docs.mongodb.org/master/reference/glossary/#term-ordered-query-plan**

        * Query Plan 修訂

            * collection 有一千次的寫入

            * reindex 重新建立index

            * 刪除某個index

            * mongod 重新啟動

        * MongoDB 在2.5.5 新增了 Query Plan Cache Methods
          延伸未讀： **http://docs.mongodb.org/master/reference/method/js-plan-cache/**

        * indexex Filters :
          延伸未讀： **http://docs.mongodb.org/master/core/query-plans/#index-filters**

Distributed Queries
""""""""""""""""""""

    分佈式查詢：延伸未讀
    **http://docs.mongodb.org/master/core/distributed-queries/**

Write Operations
^^^^^^^^^^^^^^^^^^

Overview
""""""""""

    * MongoDB 有三種 write operations ： insert, update, remove

    * 修改可以使用update()或是save()
      細節 update() : http://docs.mongodb.org/manual/reference/method/db.collection.update/#db.collection.update
      save():http://docs.mongodb.org/manual/reference/method/db.collection.save/#db.collection.save

    * 延伸未讀 ： $isolated : http://docs.mongodb.org/manual/reference/operator/update/isolated/


Write Concern
--------------

    等待新增

Index Type
--------------

    * 若沒有設定index，MongoDB 在搜尋時會將整個document 取出，造成效率低下

    * index 可以付帶query cirtieria 和 projection

Type
^^^^^^^

    Default _id

        Object _id

    Single Field

        針對某個欄位做index

    ..note ::

        Example:
        db.test.ensureIndex({score:1})

    Compound Index

        對多個欄位做index

    ..note ::

        Example:
        db.test.ensureIndex({userid:1, score:-1})

    Multikey Index

        * 針對陣列數值做index

        * MongoDB會自動建立Multikey index，不需要手動建立

        * Multikey搜尋時支援num, string兩種type，和巢狀document

        * shard keys 不能使用muti-key index


        ..note ::

            設定compound index 為{a:1, b:1}

            允許的Json

            {a: [1, 2], b: 1}

            {a: 1, b: [1, 2]}

            MongoDB若是已經設定上述compound index
            則
            {a: [1, 2], b: [1, 2]}這一筆資料會無法寫入並回傳 cannot index parallel arrays

    ..note ::

        Example:
        json {
            userid:'xxx',
            addr:[
                {zip:"10036"},
                {zip:'97455'}
            ]
        }

        Create Index

        db.test.ensureIndex({'addr.zip':1})

    Geospatial Index

        支援2d 地理空間資料index(還需要研究，這瞎小)

    Text Index

        MongoDB 支援text index，
        These text indexes do not store language-specific stop words
        (e.g. “the”, “a”, “or”) and stem the words in a collection to only store root words.

        * Text Index 不區分大小寫，Text Index 內容可以是String, array或是其他field(dict)

        * 在建立一個text index之前要先Enable Text Search

        * 一個collection 只可以有一個text index

        * Specify a Language for Text Index(針對Text Index 語言設定)

            * 預設的語言為English

            ..note ::

                Example:

                設定為單班牙語系

                db .test.ensureIndex(
                    {content:'text'},
                    {default_language:"spnish"}
                )

                設定多國語系

                Json Example:

                { _id: 1, idioma: "portuguese", quote: "A sorte protege os audazes" }
                { _id: 2, idioma: "spanish", quote: "Nada hay más surreal que la realidad." }
                { _id: 3, idioma: "english", quote: "is this a dagger which I see before me" }

                Example:

                db.test.ensureIndex(
                    {quote:'text'},
                    {language_override:'idioma'}
                )

                db.test.runCommand( "text", { search: "que", language: "spanish" } )



        ..note ::

            Example:

            可以針對某些field 設定text index

            db.test.ensureIndex(
                {
                    subject:'text',
                    content:'text'
                }
            )

            也可以針對所有field 使用"$**" 特殊字元設定所有的 text index

            db.test.ensureIndex(
                {
                    "$**":"text",
                    name : "TextIndex"
                }
            )

        ..note ::

            Enable Text Search :

            http://docs.mongodb.org/manual/tutorial/enable-text-search/

        * 每個Collection只能建立一個 Text Index

        * Text Index 需要付出的載體需求與效率注意事項

            * text indexes change the space allocation method for all future record allocations in a collection to usePowerOf2Sizes.
              (變更儲存方法，詳細延伸未讀:http://docs.mongodb.org/manual/reference/command/collMod/#usePowerOf2Sizes)

            * Text index 很佔記憶體空間

            * text index 和 巨大multi-key index是相似的，都會佔用很大的記憶體空間

            * 建立一個大的Text Index 會有很大的限制，
              延伸未讀：http://docs.mongodb.org/manual/reference/ulimit/

            * 會影響insert的效率，因為每次新增時都必須在每個新的document 加入unique post-stemmed word

            * Text Index 不要只有儲存片語或是關於document 的相關資訊？較小的Text Index 可以在RAM中直接被搜尋自然提高效率。

        * Text Search 可以對文件中的部份字串進行搜尋

    Hashed Index

        * MongoDB 支援 hashed index，但是不支援Multikey index，有支援sharding，

        * 可以接受equally query 但是不能接受 range

        ..note ::

              延伸未讀
            hashed shard key :

            http://docs.mongodb.org/manual/core/sharding-shard-key/#sharding-hashed-sharding

            shard a Collection Using a Hashed Shard Key:

            http://docs.mongodb.org/manual/tutorial/shard-collection-with-a-hashed-shard-key/


Index Properties
^^^^^^^^^^^^^^^^^^^

    Unique Indexes

        * MongoDB unique的預設值是 flase

        * 若針對compound index 中的某一個直設定為unique, MongoDB 會將這個組合做unique ，而不是對單一的值(If you use the unique constraint on a compound index, then MongoDB will enforce uniqueness on the combination of values rather than the individual value for any or all values of the key.)

        * 對某個field設定unique, 第1筆空值資料會補Null值，但是在第2筆空值時，則會出現duplicate key error, 你可以結合Sparse index 來避免這些錯誤。

        * 不能對hashed index 設定unique prototype

        ..note ::

            Example:

            db.test.ensureIndex({"user_id":1}, {unique:true})

    Sparse Indexes

Search String content for Text
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    延伸未讀：http://docs.mongodb.org/manual/tutorial/search-for-text/

Mongoengine
-------------

基本安裝與連線
^^^^^^^^^^^^^^^

    Getting Start

    安裝Mongoengine

    ::

        $ pip install mongoengine

    DEMO

    demo.py

    .. code-block :: python

        from mongoengine import connect
        connect("test")

    Users object

    .. code-block :: python

        class User(Document):
            email = StringField(required=True)
            first_name = StringField(max_length=50)
            last_name = StringField(max_length=50)

        ross = User(email='ross@example.com', first_name='Ross', last_name='Lawley').save()

    Insert Data

    DEMO

    insert.py

    .. code-block :: python

        from mongoengine import connect, StringField, Document, \
            ReferenceField, ListField, EmbeddedDocumentField, EmbeddedDocument

        connect("test")
        class User(Document):
            email = StringField(required=True)
            first_name = StringField(max_length=50)
            last_name = StringField(max_length=50)

        ross = User(email='ross@example.com')
        ross.first_name = 'Ross'
        ross.last_name = 'Lawley'
        ross.save()

    Embedding Insert

    DEMO

    embedding_insert.py

    .. code-block :: python

        from mongoengine import connect, StringField, Document, \
            ReferenceField, ListField, EmbeddedDocumentField, EmbeddedDocument

        connect("test")
        post1 = TextPost(title='Fun with MongoEngine', author=john)
        post1.content = 'Took a look at MongoEngine today, looks pretty cool.'
        post1.tags = ['mongodb', 'mongoengine']
        post1.save()

        post2 = LinkPost(title='MongoEngine Documentation', author=ross)
        post2.link_url = 'http://docs.mongoengine.com/'

        post2.tags = ['mongoengine']
        post2.save()

Field
^^^^^^^

    RefrenceField

    .. code-block:: python


        from flask import Flask, request
        from flask.ext.mongoengine import MongoEngine

        app = Flask(__name__)

        app.config.from_object('config')

        db = MongoEngine(app)

        class Foo(db.Document):
            name = db.StringField()
            pwd = db.StringField()

        class Bar(db.Document):
            test = db.StringField()
            foo = db.ReferenceField('Foo', dbref=True)

        @app.route('/', methods=['POST'])
        def index():
            """
            {"name":"tomas111", "pwd":"123456"}
            """
            data = request.get_json(force=True)
            f = Foo.objects(**data).first()
            B = Bar(test = data['name'], foo=f).save()
            B = Bar.objects(test = data['name']).first()
            return '111'

        app.run()

Querying the database
^^^^^^^^^^^^^^^^^^^^^^

pass

參考資料
"""""""""

    Querying the database: http://docs.mongoengine.org/guide/querying.html


Flask Mongoengine Configure
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    ::

        $ pip install flask-mongoengine

    .. code-block :: python

        from flask import Flask
        from flask.ext.mongoengine import Mongoengine

        app = Flask(__name__)
        app.config["MONGODB_SETTINGS"] = {"DB":"my_tumble_log"}

        db = Mongoengine(app)

    設定設定key值(DB, USERNAME, PASSWORD, HOST, PORT)

Flask Mongoengine and WTForm
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    DEMO

    .. code-block :: python

        from flask.ext.mongoengine.wtf import model_form

        class User(db.Document):
            email = db.StringField(required=True)
            first_name = db.StringField(max_length=50)
            last_name = db.StringField(max_length=50)

        class Content(db.EmbeddedDocument):
            text = db.StringField()
            lang = db.StringField(max_length=3)

        class Post(db.Document):
            title = db.StringField(max_length=120, required=True)
            author = db.ReferenceField(User)
            tags = db.ListField(db.StringField(max_length=30))
            content = db.EmbeddedDocumentField(Content)

        PostForm = model_form(Post)

        def add_post(request):
            form = PostForm(request.POST)
            if request.method == 'POST' and form.validate():

                # do something
                redirect('done')
            return render_response('add_post.html', form=form)

Support Field
^^^^^^^^^^^^^^^^

    StringField

    BinaryField

    URLField

    EmailField

    IntField

    FloatField

    DecimalField

    BooleanField

    DateTimeField

    ListField (using wtforms.fields.FieldList )

    SortedListField (duplicate ListField)

    EmbeddedDocumentField (using wtforms.fields.FormField and generating inline Form)

    ReferenceField (using wtforms.fields.SelectFieldBase with options loaded from QuerySet or Document)

    DictField


API Reference
^^^^^^^^^^^^^^^^

    class mongoengine.queryset.QuerySet(document, collection)


        insert(doc_or_docs, load_bulk=True, write_concern=None)

            Parameters::

               *  docs_or_doc – a document or list of documents to be inserted(optional)

               * (load_bulk) – If True returns the list of document instances

               * write_concern – Extra keyword arguments are passed down to insert() which
                 will be used as options for the resultant getLastError command. For example,
                 insert(..., {w: 2, fsync: True}) will wait until at least two servers have
                 recorded the write and will force an fsync on each server being written to.

        update(upsert=False, multi=True, write_concern=None, full_result=False, \**update)

            Parameters::

                * upsert – 存在的document 會複寫ObjectID

                * multi – 同時修改多個document

                * write_concern – Extra keyword arguments are passed down which will
                  be used as options for the resultant getLastError command. For example,
                  save(..., write_concern={w: 2, fsync: True}, ...) will wait until at
                  least two servers have recorded the write and will force an fsync on
                  the primary server.

                * full_result – Return the full result rather than just the number updated.

                * update – Django-style update keyword arguments


Mongoose
------------------

Install
^^^^^^^^^^

    ::

        npm install mongoose --save

    simple demo code

    .. code-block:: javascript

        var express = require('express'),
            mongoose = require('mongoose'),
            app = express();

            Cat = mongoose.model('Cat', {name: String});
            kitty = new Cat({name: 'kitty'});

        mongoose.connect('mongodb://localhost/test');

        app.get('/', function(req, res){
            kitty.save(function(err){
                if(err){
                    consolg.log('meow');

                }
            });
        res.send('success');
        });

        app.listen(3000);

Define Schema
""""""""""""""

    Demo

    .. code-block:: javascript

        var mongoose = require("mongoose"),
            Schema = mongoose.Schema;

        var blogSchema = new Schema({
            //custom error
            title:  {type:String, required:'title is required'},
            author: String,
            body:   String,
            comments: [{
                body: String,
                date: Date
            }],
            date: {
                type: Date, default: Date.now
            },
            hidden: Boolean,
            meta: {
                votes: Number,
                favs:  Number
            }
        });
        //加入其他欄位
        blogSchema.add({
            name: 'string',
            color: 'string',
            price: 'number'
        });

    Field Type

    * String 字串

    * Number 數字

    * Date 日期

    * Buffer 陣列

    * Boolean 布林

    * Mixed

    * ObjectId

    * Array 陣列

Model
"""""""

    .. code-block:: javascript

        var schema = new mongoose.schema({
                name: 'string',
                size: 'string'
            }),

            Tank = mongoose.model("Tank", schema);

            var small = new Tank({
                size: 'small'
                });

            small.save(function (err) {
                if (err) return handleError(err);
                // saved!})// or
            Tank.create({ size: 'small'},
                function (err, small) {
                    if (err) return handleError(err);
                    // saved!
            });


Query
"""""""


    .. code-block:: javascript

        //Find
        Tank.query({size: "small"}).where("createDate").gt(oneYearage).exec();

        //Delete

        Tank.remove({size: "small"}, function(err){
            if(err) {
                errorHandler(err);
            }
        });

        //update single document

        Tank.finsOneAndUpdate(condition, update, option, callback) //exec

    參考未讀： http://mongoosejs.com/docs/queries.html
                http://mongoosejs.com/docs/api.html
                http://mongoosejs.com/docs/api.html#model_Model.update





Replication
------------

    等待新增

Deploy a Replica Set
^^^^^^^^^^^^^^^^^^^^^^

    **建立三個測試的Mongo Replication**

        先停止MongoDB的服務

        ::

            $ sudo /etc/init.d/mongodb stop

        修改/etc/hosts 中的127.0.0.1 localhost 加入

        ::

            127.0.0.1 localhost m1.example.net m2.example.net m3.example.net

        開3個port 模擬三台電腦的MongoDB

        ::

            $ mongod --port 27017 --dbpath /srv/mongodb/rs0-0 --replSet rs0 --smallfiles --oplogSize 128
            $ mongod --port 27018 --dbpath /srv/mongodb/rs0-1 --replSet rs0 --smallfiles --oplogSize 128
            $ mongod --port 27019 --dbpath /srv/mongodb/rs0-2 --replSet rs0 --smallfiles --oplogSize 128

    **Shell mode**

    ::

        rsconf = {
           _id: "rs0",
           members: [
                      {
                       _id: 0,
                       host: "m1.example.net:27017"
                      }
                    ]
         }

    **增加其他MongoDB**

    ::

        rs.add("m2.example.net:27018")
        rs.add("m3.example.net:27018")

    **查看狀態**

    ::

         $ rs.status()


