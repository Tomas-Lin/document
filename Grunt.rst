Grunt
-------

    ::

        npm install -g grunt-cli
        //安裝grunt init package
        npm install -g grunt-init


Template
==========

    Path : ~/.grunt/package-name

    example :

    ::

        //Grunt Example for jquery
        git clone https://github.com/gruntjs/grunt-init-jquery.git ~/.grunt-init/jquery

參考資料
=========

    Grunt 教學：http://www.gtwang.org/2013/12/grunt-javascript-task-runner.html

    Grunt Doc:  http://gruntjs.com/

