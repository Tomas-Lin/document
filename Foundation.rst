.. sphinx documentation master file, created by
   sphinx-quickstart on Wed Apr 10 10:30:15 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Foundation
==================================

Documentation
---------------

Get Starting
^^^^^^^^^^^^^

    Install the compass gem

        install ruby

        ::

            $ sudo apt-get install ruby

            $ sudo gem install zurb-foundation

            $ sudo gem install compass

            $ npm install -g bower grunt-cli

            $ sudo gem install foundation


Get New Project
^^^^^^^^^^^^^^^^

    ::

        $ foundation new project

    config.rb

    ::


        # Require any additional compass plugins here.
        add_import_path "bower_components/foundation/scss"

        # Set this to the root of your project when deployed:
        http_path = "/"
        css_dir = "../static/stylesheets"
        sass_dir = "scss"
        images_dir = "../static/images"
        javascripts_dir = "../static/js"

        # You can select your preferred output style here (can be overridden via the command line):
        # output_style = :expanded or :nested or :compact or :compressed

        # To enable relative paths to assets via compass helper functions. Uncomment:
        # relative_assets = true

        # To disable debugging comments that display the original location of your selectors. Uncomment:
        # line_comments = false


        # If you prefer the indented syntax, you might want to regenerate this
        # project again passing --syntax sass, or you can uncomment this:
        # preferred_syntax = :sass
        # and then run:
        # sass-convert -R --from scss --to sass sass scss && rm -rf sass && mv scss sass

DEMO custom.scss
"""""""""""""""""""""

    custom.scss DEMO

    ::

        #main p {
          color: #00ff00;
          width: 97%;

          .redbox {
            background-color: #ff0000;
            color: #000000;
          }
        }

    上述經過 compiler之後的結果為

    ::

        #main p {
          color: #00ff00;
          width: 97%; }
          #main p .redbox {
            background-color: #ff0000;
            color: #000000; }

    ::

        a {
          font-weight: bold;
          text-decoration: none;
          &:hover { text-decoration: underline; }
          body.firefox & { font-weight: normal; }
        }

    compiler後

    ::

        a {
          font-weight: bold;
          text-decoration: none; }
          a:hover {
            text-decoration: underline; }
          body.firefox a {
            font-weight: normal; }

參考資料
""""""""""""""""

    官方網站 : http://foundation.zurb.com/old-docs/f3/compass.php

