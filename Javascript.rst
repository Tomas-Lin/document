.. sphinx documentation master file, created by
   sphinx-quickstart on Wed Apr 10 10:30:15 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Javascript
==================================

Basic
---------------------------------------------------------------

    HTTP access control

    參考資料：https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest?redirectlocale=en-US&redirectslug=DOM%2FXMLHttpRequest

    .. code-block : javascript

        var invocation = new XMLHttpRequest();
        var url = 'http://bar.other/resources/public-data/';

        function callOtherDomain() {
          if(invocation) {
            invocation.open('GET', url, true);
            invocation.onreadystatechange = handler;
            invocation.send();
          }
        }

Browserify
-----------

Install
^^^^^^^^

    ::

        npm install -g browserify


Coordinate System
---------------------

Javascript 攥寫座標系統紀錄
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    座標系統 topic

    .. note ::

        定義座標系統的X軸與Y軸

        新增的tag會產生在座標系統上定位

        取得標籤的X, Y 座標

    .. code-block :: html

        <!DOCTYPE html>
       <html lang="en">
       <head>
           <meta charset="UTF-8">
           <title>Coordinate System</title>
       </head>
        <body>
            <div id='coordinate'  style='width:800px;height:600px;border:1px solid #CCC;'>
        <div id='test' style='position:relative;'>test</div>
        </div>
                <input type="number" id="x">
                <input type="number" id="y">
                <button id="move">移動</button>
        </body>

        <script src="jquery.min.js"></script>

        <script>
            $(function(){

                function Coordinate(id, LR_offset, TB_offset){
                    this.convas = $(id);
                    this.width = this.convas.width();
                    this.height = this.convas.height();
                    this.LR_offset = this.width / LR_offset;
                    this.TB_offset = this.height / TB_offset;
                }

                Coordinate.prototype.get_X = function(x){
                    return this.LR_offset * x;
                }

                Coordinate.prototype.get_Y = function(y){
                    return this.TB_offset * y;
                }


                var coordinate_box = new Coordinate('#coordinate', 8, 6);
                $("#move").bind("click", function(){
                        var x = coordinate_box.get_X($("#x").val()),
                            y = coordinate_box.get_Y($("#y").val());
                        $('#test').css('left', x+'px');
                        $('#test').css('top', y+'px');
                });

            });
        </script>

    </html>


    .. note ::

        鍵盤上下左右evnt

        標籤再座標中進行上下左右移動


Time Sync Server
-----------------

    時間同步系統 topic

    ..note ::

        抓取 NTP 時間

        倒數計時 發送 request

React
--------

jsx
^^^^

    SPEC: http://facebook.github.io/jsx/

官方資料
^^^^^^^^^

Document : http://facebook.github.io/react/index.html

Dart
--------

    v8 引擎目前負責大部分的Chrome的速度，但是有許多V8 引擎的工程師目前正在研究Dart

    是物件導向的程式語言

    Dart 可以在瀏覽器中執行，也可以獨立出來運行，你可以直接使用編輯器編寫Dart，並且啟動與調校Dart應用程序

Quckly Start
^^^^^^^^^^^^^^

    Download and Install the software

    ::
        #64 bit

        $ wget http://storage.googleapis.com/dart-archive/channels/stable/release/latest/editor/darteditor-linux-x64.zip

        #32 bit

        $ wget http://storage.googleapis.com/dart-archive/channels/stable/release/latest/editor/darteditor-linux-ia32.zip

    unzip

    ::

        $ unzip darteditor-linux-x64.zip

    get the sample code

    ::

        $ wget https://github.com/dart-lang/one-hour-codelab/archive/master.zip

    .. note ::

        Dart 初次使用時可能會有Bug，因為libc6的套件版本問題，libc6的版本需要在2.15以上，但是debain的版本是2.13
        所以需要自己去安裝2.15以上的libc6。

    ::

        ##確認libc6的版本，若版本為2.15以上的話則不需要以下步驟
        $ sudo aptitude show libc6

        $ sudo echo 'deb http://ftp.debian.org/debian sid main' >> /etc/apt/sources.list

        $ sudo apt-get update

        $ sudo apt-get -t sid install libc6 libc6-dev libc6-dbg
        ##將deb http://ftp.debian.org/debian sid main 註解##
        $ sudo vim /etc/apt/sources.list

        $sudo apt-get update


