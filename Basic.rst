.. sphinx documentation master file, created by
   sphinx-quickstart on Wed Apr 10 10:30:15 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   
Todo
==================================

程式基礎
------------------------------

python Virtualenv
^^^^^^^^^^^^^^^^^^

  ::

    $ export WORKON_HOME=$HOME/.virtualenvs
    $ export PROJECT_HOME=$HOME/Devel
    $ export VIRTUALENVWRAPPER_SCRIPT=/usr/local/bin/virtualenvwrapper.sh
    $ source /usr/local/bin/virtualenvwrapper_lazy.sh



變數
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    可以視為在電腦記憶體中可以修改必且存在值得命名空間。

    當宣告某變數開始直譯器或是編譯器會設定一個空間來存放值，若變數不再使用時可以釋放。

    變數通常會有各自的型別，主要可以分為int，float和string，當然還有其他依照程式語言的不同會有不同的型別，這部份就各自依照要學習的程式語言作更深入的探討。

    變數的命名必須尤英文或是底線的組合，並且盡量以有意義的方式命名。


陣列
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    類似數學的矩陣，中必須先向記憶體請求儲存空間，此空間大小無法改變，否則會有溢位的問題產生。

    陣列是有連續性的，中間不會存在其他程式需要調用的數據。


物件
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

git basic
^^^^^^^^^^^^^^^

    git checkout -b jsmove(name)

    git add 隨時可以增加

    git commit 完成一個功能測試完成後

    git branch -D jsmove(刪除分支)



