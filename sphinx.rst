Sphinx 使用紀錄
------------------

vim Plugin
^^^^^^^^^^^

參考資料：
""""""""""

github: https://github.com/Rykka/riv.vim

video:  http://www.youtube.com/watch?v=sgSz2J1NVJ8



Basic
^^^^^^
    Table:

    ::

        +------------------------+-----------------------------------------+
        | BooleanField           | reuturn a tag <input type="checkbox">   |
        +========================+=========================================+
        | DateField              | default：%Y-%m-%d                       |
        +------------------------+-----------------------------------------+
        | DateTimeField          | default：%Y-%m-%d %H:%M:%S              |
        +------------------------+-----------------------------------------+

    .. note::

        若內容為中文，因為儲存長度為一般的兩倍，所以必須要將長度加長

    List:

