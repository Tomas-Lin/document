.. sphinx documentation master file, created by
   sphinx-quickstart on Wed Apr 10 10:30:15 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Nodejs
========

glupjs
------

    客製化的自動工具

    依照glupfile.js中的程式依序執行

    減少重複的動作

Install
^^^^^^^^

    ::

        pip install gulp

glup API document
^^^^^^^^^^^^^^^^^^

HTML Minification
""""""""""""""""""

    ::

        npm install --save-dev gulp-minify-html



    .. code-block:: javascript

        var gulp= require('gulp'),
            minifyHtml = require('gulp-minify-html');

        gulp.task('minify-html', function(){
            gulp.src('./Html/*.html')
            .pipe(mknifyHtml())
            .pipe(gulp.dest('build/templates'))
        });

Css Minification
"""""""""""""""""

    ::

        npm install --save-dev gulp-minify-css

    .. code-block:: javascript

        var gulp = require('gulp'),
            minifyCss = require('gulp-minify-css');

        gulp.task('minify-css', function(){
            gulp.src('./Css/one.css')
            .pipe(minifydCss())
            pipe(gulp.dest('build/static/css'));
        });

    run

    ::

        gulp minify-css

    gulp.src(globs[,options])

    options:

        options.buffer

            Default: true

            Type: Boolean

            set false時，檔案不是buffer files時會回傳file.contents

        options.read

            Default: true

            Type: Boolean

            set false 的話，若是讀取檔案為null 不會試圖開啟檔案

        options.base

            Type: String

        sample

        .. code-block:: javascript

            gulp.src('client/js/**/*.js')
                .pipe(minify())
                .pipe(gulp.dest('build'));

            gulp.src('client/js/**/*.js', {base:'client'})
                .pipe(minify())
                .pipe(gulp.dest('build'));

    gulp.dest(path[,options])

        將檔案複製到某一個目的地資料夾

        path

            Type: String or Function

            目的地資料夾的路徑

        options

            options.cwd

                Type: String

                cwd for the output folder, only has an effect if provided output folder is relative.

            options.mode

                Type: String

                Default: 0777

                檔案的權限設定

Defferred
----------

    傳統的callback function

    .. code-block:: javascript

        var query = function(task, callback){
            var count = 0;
            count++;
            console.log(count);
            return callback(task + " finished");
        }
        query("task1", function(msg){
            console.log(msg);
            return query("task2", function(msg){
                console.log(msg);
                return query("task3", function(msg){
                    return console.log(msg);
                });
            });
        });

    node deferred

    .. code-block:: javascript

        var deferred = require("deferred");
        var query_dfr = function(task){
            var _dfr;
            _dfr = deferred();
            _dfr.resolve(task);
            return _dfr.promise();
        }

        query_dfr("task1").done(function(msg){
            console.log("Deferred"+msg);
            query_dfr("task2").done(function(msg){
                console.log("Deferred"+msg);
                query_dfr("task3").done(function(msg){
                    console.log("Deferred"+msg);
                });
            });
        });

    Sample

    .. code-block:: javascript

        var deferred = require("deferred");
        var query_dfr = function(task){
            var _dfr;
            _dfr = deferred();
            _dfr.resolve(task);
            return _dfr.promise();
        }

        query_dfr("task1").then(function(msg){
            console.log('Deferred ' + msg);
            return query_dfr("task2");
        }).then(function(msg){
            console.log('Deferred ' + msg);
            return query_dfr("task3");
        }).done(function(msg){
            console.log('Deferred ' + msg);
        });

Express
--------

    Express-genaretor

    ::

        $ express

    express-jade Hello World example

    .. code-block:: javascript

        var express = require("express"),
            app = express();

        app.set("views", __dirname + "/views");
        app.set("view engine", "jade");

        app.get("/", function(req, res){
            res.render("index");
        });

        app.listen(3000, function(){
            console.log("Listenning to *:3000");
        });

    /views/index.jade

    .. code-block:: jade

        doctype html
        html
            head
                title Sample
            body
                | Hello World!!

BodyPaser
^^^^^^^^^^

    Simple Demo

    .. code-block:: javascript

        var app = require("express")(),
            bodyPaser = require("body-parser");

        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded());

    urlencoded() parse GET args?

Lo-Dash
--------

Install
^^^^^^^^

    ::

        $ npm install --save lodash

Browser Side
^^^^^^^^^^^^^

    .. code-block:: html

        <script src="lodash.js"></script>

Nodejs Side
^^^^^^^^^^^^^

    .. code-block:: javascript

        var wrapped = _([1, 2, 3]);

        // returns an unwrapped value
        wrapped.reduce(function(sum, num) {
        return sum + num;
        });
        // → 6

        // returns a wrapped value
        var squares = wrapped.map(function(num) {
        return num * num;
        });

        _.isArray(squares);
        // → false

        _.isArray(squares.value());
        // → true


    .. code-block:: javascript

        var characters = [
            { 'name': 'barney', 'age': 36 },
            { 'name': 'fred',   'age': 40 }
        ];

        // without explicit chaining
        _(characters).first();
        // → { 'name': 'barney', 'age': 36 }

        // with explicit chaining
        _(characters).chain()
        .first()
        .pick('age')
        .value();
        // → { 'age': 36 }

    .. code-block:: javascript

        var _ = require('lodash');

        _(([1,2,3]).toString();
        //1,2,3

參考資料
^^^^^^^^^^^

    Lo-Dash_

.. _Lo-Dash: https://lodash.com/

Mocha
------

Install
^^^^^^^^^

    ::

        npm install -g mocha

        mocha --version

    install assertion   library

    ::

        //sould.js 檢查是否為期待值得lib
        //except.js 檢驗except 的期待值
        //chai支持以上兩種語法的lib
        //較推薦chai

        npm install chai




Socket.io
-----------

Connect
^^^^^^^^

    Demo

    .. code-block:: javascript

        #server.js
        var app = require("express")(),
        http = require("http").Server(app),
        io = require("socket.io")(http);

        io.on('connection', function(socket){
            console.log("Hello World!");
        });

Emit
^^^^^

    Demo

    ..code-block:: javascript

        io.on('connection', function(socket){

            socket.on('room', function(room){

                socket.join(room);

            });

            socket.broadcast.emit("a user connected");

            socket.on('chat message', function(msg){

                io.emit('chat message', msg);

            });

            socket.on("disconnect", function(){

                socket.broadcast.emit("a user disconnected");

            });

        });

        #針對room中的user 發送emit

        app.get("/", function(req, res){

            res.sendfile("index.html");
            io.sockets.in(roomID).emit("message", "You are in abc123")

        });





Basic Skill
-------------

    wait

系統資料
^^^^^^^^^

    Linux: cruchbang(Debian)

    nodejs: 0.10.26

    tarball: http://nodejs.org/dist/v0.10.26/node-v0.10.26.tar.gz

    express: 4.0.0(3.5.X與4.0.0是大改版！需注意版本號碼)

    ::

        npm install npm install express@4.0.0


Heroku | 10 Habits of a Happy Node Hacker
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    npm init 初始化APP

    ::

        mkdir my-node-app
        cd my-node-app
        npm init

    紀錄所有的相關性套件

    ::

        npm install tool-package --save

    設定start script in package.json

    ::

        "scripts": {  "start": "node index.js"}

    設定test script in package.json

    ::

        "scripts": {  "test": "mocha"}

    將相依性套件排除再版本控制系統之外

    ::

        echo node_modules >> .gitignore

        Use environment variables to configure npmFrom (還沒測試搞懂)

           the npm config docs:Any environment variables that start with npm_conf
           ig_ will be interpreted as a configuration parameter. For example, putting npm_config_foo=bar in your environment will set the foo configuration parameter to bar. Any environment configurations that are not given a value will be given the value of true. Config values are case-insensitive, so NPM_CONFIG_FOO=bar will work the same.As of recently, app en
           vironment is available in all Heroku builds. This change gives node users on Heroku control over their npm configuration without having to make changes to application code. Habit #7 is a perfect example of this approach.


        Bring your own npm registryThe public npm registry has seen immense(忘記heroku怎麼搞 所以有空再測試)

            growth in recent years, and with that has come occasional instability. As a result, many node users are seeking alternatives to the public registry, either for the sake of speed and stability in the development and build cycle, or as a means to host private node modules.A number or alter
             native npm registries have popped up in recent months. Nodejitsu and Gemfury offer paid private registries, and there are some free alternatives such as Mozilla's read-only S3/CloudFront mirror and Maciej Małecki's European mirror.Configuring your Heroku node app
             to use a custom registry is easy:

        控管dependencies

    ::

        cd my-node-app
        npm outdated

    利用npm scripts 執行相關程式

    ::

        {
            "name": "my-node-app",
            "version": "1.2.3",
            "scripts": {
                "preinstall": "echo here it comes!",
                "postinstall": "echo there it goes!",
                "start": "node index.js",
                "test": "tap test/*.js"
            }
        }

    OR

    ::

        {
            "scripts": {
                "postinstall": "npm run build && npm run rejoice",
                "build": "grunt",
                "rejoice": "echo yay!",
                "start": "node index.js"
            }
        }

    多嘗試新事物

    Have fun!!

    參考資料： https://blog.heroku.com/archives/2014/3/11/node-habits

package.json
^^^^^^^^^^^^^^

    * 設定APP的設定檔案

    demo

    package.json

    .. code-block :: json

        {
            "name": "node_mssql_demo",
            "version": "0.0.1",
            "description": "Nodejs connect to MsSQL",
            "main": "app.js",
            "dependencies": {
                "mssql": "~0.5.3",
                "node-mssql-connector": "^0.2.3",
                "tds": "~0.1.0",
                "express": "^4.3.1",
                "jade": "^1.3.1",
                "method-override": "^1.0.2",
                "body-parser": "^1.2.1",
                "mocha": "^1.19.0"
            },
            "devDependencies": {},
            "scripts": {
                "test": "mocha",
                "start": "node app.js"
            },
            "keywords": [
                "Node.js",
                "MsSQL"
            ],
            "author": "Tomas",
            "license": "ISC"
        }


Connect to Youtube Api
^^^^^^^^^^^^^^^^^^^^^^^^

Nodejs Connect to Youtube
--------------------------

    app.js:

    .. code-block :: javascript

        var express = require('express'),
            mongoose = require('mongoose'),
            request = require('request'),
            app = express(),
            ytb = require("youtube-api"),
            credentials = require("./credentials"),
            authUrl = "https://accounts.google.com/o/oauth2/auth?";

        mongoose.connect('mongodb://localhost/test');


        for (var key in credentials) {
            if(key ==='client_secret'){ continue }
            authUrl += "&" + key + "=" + credentials[key];
            }
        app.set('view engine', 'jade');


        app.get('/', function(req, res){
            res.writeHead(302, {
                "Location": authUrl
            });
            res.end();
        });

        app.get('/oauth2callback', function(req, res){
            var url = 'https://accounts.google.com/o/oauth2/token',
                data = {form:{code: req.query.code,
                    client_id: credentials.client_id,
                    client_secret: credentials.client_secret,
                    redirect_uri: credentials.redirect_uri,
                    grant_type: 'authorization_code'}}


            console.log(req.query.code);
            request.post(url, data, function(err, resp, body){
                if(err){
                    console.log(err);
                }else{
                    data = JSON.parse(resp.body);
                    ytb.authenticate({
                        "type": "oauth",
                        "token": data.access_token
                    });
                    ytb.playlists.list({
                        "part": "snippet",
                        "id":   "PLHzXa_F24ltIiKLvIab-ZWUMQZxLoiLa6",
                        "mySubscribers": true,
                        "maxResults": 50
                    }, function(err, data){
                        console.log(err);
                    });

                }
            });
            res.redirect('/singlelist');

        });

        app.get('/singlelist', function(req, res){
            res.send('hello')
        });
        app.listen(6555);


參考資料
^^^^^^^^^

    DOC: https://developers.google.com/youtube/

    oauth login: https://developers.google.com/youtube/2.0/developers_guide_protocol_oauth2

    demo git repositories : :https://bitbucket.org/Tomas-Lin/ytb-oauth-login/src/0736c8885b17412fdcbe2e5b47813158cf9a77f0/app.js?at=master
